import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import {MenuComponent} from "../components/menu/menu";
import { DayPageModule } from '../pages/day/day.module';
import { HTTP} from '@ionic-native/http'
import { HttpModule } from '@angular/http';
import { PipesModule } from '../pipes/pipes.module';
import { MonthPage } from '../pages/month/month';
import { ComponentsModule } from '../components/components.module';
import { TodayPage } from '../pages/today/today';
// import { SegmentsListComponent } from '../components/segments-list/segments-list';


@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    MonthPage,
    TodayPage
    // SegmentsListComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    DayPageModule,
    PipesModule,
    HttpModule,
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    MonthPage,
    TodayPage
    // SegmentsListComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HTTP,
    // SegmentsListComponent,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ],

})
export class AppModule {}
