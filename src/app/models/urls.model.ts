import { Injectable } from '@angular/core';
import { AppConfig } from "../config";


@Injectable()
/**
 * Класс урлов для Django REST.
 */
export class UrlModels {
  // строка для обозначение префикса сервиса 1С

  public static service1c = 'service1c';

  public static cua = 'cua';


  // Список всех магазинов
  public static apiGetShopsList = AppConfig.service1c + '/shops?onlySetOlap=true';
  // Данные по магазину и всем сегментам за период
  public static apiTotalShopReport = AppConfig.api +  '/totalshopreport';
   // Возвращает список сегментов из базы sqlite
  public static apiGetSegments = AppConfig.api + '/getsegmentsfromsqlite';
   //  Возвращает список всех сегментов магазина
  public static apiGetSegmentsList = AppConfig.service1c + '/segments';
   // Вносит список сегментов в базу sqlite
  public static apiSetSegments = AppConfig.api + '/insertsegments';
   // Данные по магазину и сегменту за период
  public static apiShopReport = AppConfig.api + '/shopreport';
   // Возвращает какой процент от плана по товарообороту должен быть выполнен к определенному времени и дню недели
  public static apiGetPlanExecution = AppConfig.api + '/getplanexecution';
  // Данные по всем магазинам за сегодня - количество чеков и ТО
  public static apiSummaryCashReport =  AppConfig.api + '/allsummarytoday';
   // Возвращает день в прошлом году, соответствующий выбранному
  public static apiGetDay = AppConfig.api + '/getday';
   // Данные с касс за сегодня по всем магазинам
  public static apiShopReportToday = AppConfig.api + '/shopstoday';
  // Данные по сегментам магазина
  public static apiSaleReport = AppConfig.api + '/salereport';
}
