import { ModuleWithProviders, NgModule} from '@angular/core';
@NgModule({
	declarations: [],
	imports: [],
	exports: [],
  providers: [
  ]
})
export class PipesModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: PipesModule,
      providers: [
      ]
    }
  }
}
