import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the NumFormatPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'numFormat',
})
export class NumFormatPipe implements PipeTransform {
 transform(value: any, args?: any): any {
    let toFixedVal = parseInt(args.slice(-1), 10);
    if (!value) {
      return 0
    } else if (args === 'toFixed' + toFixedVal) {
      if (!parseFloat(value)) {
        return value
      } else {
        return value.toFixed(toFixedVal) || 0;
      }
    } else if (args === 'numSpace') {
          if (typeof (value) === 'number') {
            value = String (value);
          }
          return value.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        } else {
          if (args === 'parseInt') {
            return parseInt(value);
          }
        }
      }
}
