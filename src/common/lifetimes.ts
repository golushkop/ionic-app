export enum Lifetimes {
    Day = 24,
    Week = 168, //  24 * 7
    Month = 5040, // 168 * 30,
    QuartHour = 0.25,
    WorkDay = 9
}
