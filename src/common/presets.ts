import { Hash } from './hash.enum';
import { Lifetimes } from './lifetimes';

export class Presets {

    static defaultSettings = {
        useHash: null,
        lifetime: null
    };

    static settingsWithLocalStorage = {
        useHash: Hash.localStorage,
        lifetime: Lifetimes.Day
    };

    static settingsWithLocalStorageForWeek = {
        useHash: Hash.localStorage,
        lifetime: Lifetimes.Week
    };

    static settingsWithLocalStorageForMonth = {
        useHash: Hash.localStorage,
        lifetime: Lifetimes.Month
    };

    static settingsWithServiceStorage = {
        useHash: Hash.storageService
    };

    static settingsWithServiceStorageForQuotHor = {
        useHash: Hash.localStorage,
        lifetime: Lifetimes.QuartHour
    };
    static settingsWithServiceStorageForWorkDay = {
        useHash: Hash.localStorage,
        lifetime: Lifetimes.WorkDay
    }
}
