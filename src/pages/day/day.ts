import {Component, QueryList, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import { GetShopListService } from '../../services/get-shop-list.service';
import { ShopReportDataService } from '../../services/shop-report-data.service';
import { RequestBlockScreenService } from '../../services/request-block-screen.service';
import { GetTotalShopReport } from '../../services/get-total-shop-report.service';
import { GetShopInfoBySegments } from '../../services/get-shop-info-by-segments.service';
import {GetShopRepBySegmService} from "../../services/get-shop-rep-by-segm.service";
import {SegmentsListComponent} from "../../components/segments-list/segments-list";
import {GetSegmentsListService} from "../../services/get-segments-list.service";
import {SetSegmentsListService} from "../../services/set-segments.service";
import {GetSqliteSegmentsListService} from "../../services/get-sqlite-segments.service";

/**
 * Generated class for the DayPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-day',
  templateUrl: 'day.html',
})
export class DayPage {
  private shopsList: Array<Object>;
  private params: Object;
  private _dateTo: string;
  private _dateFrom: string;
  private yesterday: any;
  private _date: any;
  private _dateComp: string;
  public shopsData: Array<any>;
  public shopMode: string;
  public tableVals: Array<Object>;
  public load: boolean;
  public colorTest: string = 'light';
  public buttons: Array<Object> = [{'icon': 'checkmark', 'color': 'light', 'name': 'ТО'}];
  public dpMonthNames: Array<string> = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ];
  public dpShortMonthNames: Array<string> = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек' ];
  private counter: number;
  public segmSwitch: boolean;
  public dinamicChart: boolean;
  public segmentButColor: string;
  public sumData: Array<Object>;
  public chartVal: string;




  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private _getShopsListService: GetShopListService,
              public _shopReportDataService: ShopReportDataService,
              public _requestBlockScreenService: RequestBlockScreenService,
              private platform: Platform,
              private _getTotalShopReport: GetTotalShopReport,
              private _getShopInfoBySegm: GetShopInfoBySegments,
              private _getShopRepBySegm: GetShopRepBySegmService,
              public _getSegmSqlite: GetSegmentsListService,
              public _setSegm: SetSegmentsListService,
              private _getSegm: GetSqliteSegmentsListService
  ) {
    platform.ready().then(() => {
      this.prepareDate()
    })
  }

  private prepareDate() {
    this._date = new Date();
    this.yesterday = new Date();
    this.yesterday.setDate(this.yesterday.getDate() - 1);
    this._dateTo = this.yesterday.getFullYear() + '-' + ('0' + (this.yesterday.getMonth() + 1)).slice(-2) +
          '-' + ('0' + this.yesterday.getDate()).slice(-2);
    if (!this._shopReportDataService.maxDay) {
      this._shopReportDataService.maxDay = this._dateTo;
    }
    this._dateFrom = this._dateTo;
    let today = this._date.getFullYear() + '-' + ('0' + (this._date.getMonth() + 1)).slice(-2) +
          '-' + ('0' + this._date.getDate()).slice(-2);
    if (!this._shopReportDataService._dateChosen || this._shopReportDataService._dateChosen === today) {
      this._shopReportDataService._dateChosen = this._dateTo
    };
    this.prepareParams()
  }
  private pickDate() {
    this._dateTo = this._shopReportDataService._dateChosen;
    this._dateFrom = this._shopReportDataService._dateChosen;
    this.prepareParams();

  }

  private prepareParams() {
    this.load = true;
    this.segmentButColor = 'secondary';
    this.counter = 0;
    this.tableVals = [];
    this.shopMode = this.shopMode ? this.shopMode : 'ТО';
    this.defineTableVal();
    this.defineMenuButtons();
    // this.load = true;
    this._dateComp = 'LFL';
    this.shopsList = [];
    this.shopsData = [];
    this.sumData = [];
    this._shopReportDataService._planChartToTime = false;
    this._shopReportDataService._dateSelector = 'day';
    this._shopReportDataService._segmentChosen = this._shopReportDataService._segmentChosen ? this._shopReportDataService._segmentChosen : {};
    if (this._shopReportDataService._optSelect === 'shop') {
       if (!this._shopReportDataService._segmentsList) {
         this.getSegmentsSqlite()
       }
    }
    this.getShopsList();
  }

  private defineTableVal() {
    this.tableVals = [
      {'mode': 'ТО', 'headers':['ГМ', 'ТО', 'LFL'], 'values': ['name', 'summa', 'summa_prog']},
      {'mode': 'Проникновение', 'headers':['ГМ', 'Проникновение', 'LFL'], 'values': ['name', 'customer_part', 'customer_part_prog']},
      {'mode': 'Клиенты', 'headers':['ГМ', 'Клиенты', 'LFL'], 'values': ['name', 'customer', 'customer_prog']},
      {'mode': 'Артикулы', 'headers':['ГМ', 'Артикулы', 'LFL'], 'values': ['name', 'quantity', 'quantity_prog']},
      {'mode': 'Маржа', 'headers':['ГМ', 'Маржа', 'LFL'], 'values': ['name', 'margin', 'margin_prog']},
    ];
    let self = this;
    if (this.shopsData) {
      this.shopsData.sort(function (a, b): number {
        return self.sortTableValues(a, b)
      })
    }
  }
  private defineMenuButtons() {
    this.buttons = [
      {'icon': 'done-all', 'color': 'secondary', 'name': 'Проникновение'},
      {'icon': 'people', 'color': 'secondary', 'name': 'Клиенты'},
      {'icon': 'logo-usd', 'color': 'primary', 'name': 'ТО'},
      {'icon': 'basket', 'color': 'secondary', 'name': 'Артикулы'},
      {'icon': 'calculator', 'color': 'secondary', 'name': 'Маржа'},
      {'icon': 'stats', 'color': 'secondary', 'name': 'График'},
      // {'icon': 'menu', 'color': 'secondary', 'name': 'Сегменты'},
    ]
  }
  private getShopsList() {
    this._getShopsListService.getShopsList()
      .then(
        (result: any) => {
          let self = this;
          result = JSON.parse(result);
          console.log(result);
          result['shops'].forEach(function (item: any) {
            self.shopsList.push({
              'olapCode': item.olapCode,
              'name': item.name,
              'shopNumb': parseInt(item.name.slice(-2))
            })
          });
          this._shopReportDataService._shopsList = this.shopsList;
          this._requestBlockScreenService.counter = 0;
          this._requestBlockScreenService.maxCounter = this._shopReportDataService._shopsList.length;
          this.getTotalShopData();
        },
      ).catch((e)=> {
      console.log('Ошибка загрузки списка магазинов')
    })
  }


  public renewPage() {
    alert('renew');
    this.prepareParams()
  }

  private getTotalShopData() {
    if (this.counter === this._shopReportDataService._shopsList.length) {
      console.log('===========');
      console.log(this.shopsData);
      this.counter = 0;
      // this._shopReportDataService._optSelect = 'all';
      this.calculateValues();
    } else {
      this.params = {
        'olapCode': this._shopReportDataService._shopsList[this.counter]['olapCode'],
        'dateFrom': this._dateFrom,
        'dateTo': this._dateTo,
        'dateComp': this._dateComp,
    };
      this._getTotalShopReport.get(this.params)
        .then(
          (res: any) => {
            res = JSON.parse(res);
            res.data[0]['name'] = this._shopReportDataService._shopsList[this.counter]['name'];
            res.data[0]['shopNumb'] = this._shopReportDataService._shopsList[this.counter]['name'].slice(-2);
            res.data[0]['olapCode'] = this._shopReportDataService._shopsList[this.counter]['olapCode'];
            this.shopsData.push(res.data[0]);
            this.counter++;
            this._requestBlockScreenService.counter ++;
            this.getTotalShopData();
          },
          (err: any) => {
            this.shopsData.push({
              'data': [{}],
              'errMsg': err
            });
            this.counter++;
            this._requestBlockScreenService.counter ++;
            this.getTotalShopData();
          }
        );
    }
  }

   private getSegmentShopData() {
    if (this.counter === this._shopReportDataService._shopsList.length) {
      this.counter = 0;
      // this._shopReportDataService._optSelect = 'segment';
      this.calculateValues();
    }
    else {
      this.params = {
        'olapCode': this._shopReportDataService._shopsList[this.counter]['olapCode'],
        'dateFrom': this._dateFrom,
        'dateTo': this._dateTo,
        'dateComp': this._dateComp,
        'segmentCode': this._shopReportDataService._segmentChosen['wareCode']
      };
      this._getShopInfoBySegm.get(this.params)
        .then(
          (res: any) => {
            res = JSON.parse(res);
            if (!res.errMsg) {
              res.data[0]['name'] = this._shopReportDataService._shopsList[this.counter]['name'];
              res.data[0]['shopNumb'] = this._shopReportDataService._shopsList[this.counter]['name'].slice(-2);
              res.data[0]['olapCode'] = this._shopReportDataService._shopsList[this.counter]['olapCode'];
              this.shopsData.push(res.data[0]);
            }
            else {
              this.shopsData.push({
                'name': this._shopReportDataService._shopsList[this.counter]['name'],
                'shopNumb': this._shopReportDataService._shopsList[this.counter]['name'].slice(-2),
                'olapCode': this._shopReportDataService._shopsList[this.counter]['olapCode'],
                'errMsg': res.errMsg
              });
            }
            this._requestBlockScreenService.counter++;
            this.counter++;
            this.getSegmentShopData();
          },
        ).catch((e) => {
        console.log(e)
      })
    }
  }

   private getShopChosenData() {
    if (this.counter === this._shopReportDataService._segmentsList.length) {
      this.calculateValues();
    } else {
      this.params = {
        dateFrom: this._dateFrom,
        dateTo: this._dateTo,
        olapCode: this._shopReportDataService._shopChosen['olapCode'],
        segment_code: this._shopReportDataService._segmentsList[this.counter]['wareCode'],
        dateComp: this._dateComp
      };
      this._getShopRepBySegm.get(this.params)
        .then(
          (res: any) => {
            res = JSON.parse(res);
            if (res.data !== null && res.data[0]) {
              this.shopsData[this.counter] = res.data['0'];
              this.shopsData[this.counter]['segmentName'] =
                this._shopReportDataService._segmentsList[this.counter]['segmentName'];
              this.shopsData[this.counter]['wareCode'] =
                this._shopReportDataService._segmentsList[this.counter]['wareCode'];
            }
            else {
               this.shopsData[this.counter] = {};
               this.shopsData[this.counter]['segmentName'] = this._shopReportDataService._segmentsList
                 [this.counter]['segmentName']
            }
            this._requestBlockScreenService.counter++;
            this.counter++;
            this.getShopChosenData();
            },
        ).catch((err: any) => {
        console.log('Не получилось получить данные по сегментам магазина')
      });
    }
  }

  private calculateValues() {
    // this.load = false;
    let self = this;
    this.shopsData.sort(function (a: Object, b: Object): number {
      return self.sortTableValues(a, b)
    });
    if (this.shopsData[0]['Date']) {
      this._shopReportDataService._curTime = this.shopsData[0]['Date'].slice(-5);
    }
    let sumTo: number = 0;
    let sumToBfr: number = 0;
    let sumClnt: number = 0;
    let sumClntBfr: number = 0;
    let sumArt: number = 0;
    let sumArtBfr: number = 0;
    let sumToFct: number = 0;
    let sumClntFct: number = 0;
    let sumArtFct: number = 0;
    let marginSum: number = 0;
    let lflddsum: number = 0;
    let lflddquantity: number = 0;
    let lflddcustomer: number = 0;
    let lflddsumBfr: number = 0;
    let lflddquantityBfr: number = 0;
    let lflddcustomerBfr: number = 0;
    this.shopsData.forEach(function (item: any) {
     sumTo += parseInt(item.summa) ? parseInt(item.summa) : 0;
     sumClnt += item.customer ? item.customer : 0;
     sumArt += item['quantity'] ? item['quantity'] : 0;
     sumToBfr += item.summa / (item.summa_prog / 100 + 1) ? item.summa / (item.summa_prog / 100 + 1) : 0;
     sumClntBfr += item.customer / (item.customer_prog / 100 + 1) ? item.customer / (item.customer_prog / 100 + 1) : 0;
     sumArtBfr += item['quantity'] / (item.quantity_prog / 100 + 1) ? item['quantity'] / (item.quantity_prog / 100 + 1) : 0;
     sumToFct += ((item.summa * 100) / item.summa_plan) ? ((item.summa * 100) / item.summa_plan) : 0;
     sumClntFct += (item.customer * 100 / item.customer_plan) ? (item.customer * 100 / item.customer_plan) : 0;
     sumArtFct += ((item['quantity'] * 100) / item.quantity_plan) ? ((item['quantity'] * 100) / item.quantity_plan) : 0 ;
     marginSum += (item['summa'] * item['margin']) ? (item['summa'] * item['margin']) : 0;
     lflddsum += parseInt(item.LFLDDSum) ? parseInt(item.LFLDDSum) : 0;
     lflddquantity += item['LFLDDQuantity'] ? item['LFLDDQuantity'] : 0;
     lflddcustomer += item.LFLDDCustomer ? item.LFLDDCustomer : 0;
     lflddsumBfr += item.LFLDDSum / (item.LFLDDSum_prog / 100 + 1) ? item.LFLDDSum / (item.LFLDDSum_prog / 100 + 1) : 0;
     lflddquantityBfr += item['LFLDDQuantity'] / (item.LFLDDQuantity_prog / 100 + 1) ? item['LFLDDQuantity'] / (item.LFLDDQuantity_prog / 100 + 1) : 0;
     lflddcustomerBfr += item.LFLDDCustomer / (item.LFLDDCustomer_prog / 100 + 1) ? item.LFLDDCustomer / (item.LFLDDCustomer_prog / 100 + 1) : 0;
    });
    this.sumData = [
      {'mode': 'ТО',
      'data':['\u03A3', sumTo, (sumTo - sumToBfr) / sumToBfr * 100]},
      {'mode': 'Клиенты',
      'data':['\u03A3', sumClnt, (sumClnt - sumClntBfr) / sumClntBfr * 100]},
      {'mode': 'Артикулы',
      'data':['\u03A3', sumArt, (sumArt / sumArtFct) * 100]},
      {'mode': 'Маржа',
      'data':['\u03A3', marginSum / sumTo, '=====']}];
    console.log(this.sumData);
    this.load = false;
  }

  public changeMode(value) {
    let self = this;
    this.buttons.forEach(function (but) {
      if (value === but['name']) {
        but['color'] = 'primary';
        self.shopMode = but['name'];
      } else {
        but['color'] = 'secondary'
      }
    });
    if (this.shopMode === 'График') {
      this._shopReportDataService._shopMode = 'ПланПериод'
    }
    this.defineTableVal();
  }

  public dateIsChanged() {
    this.pickDate()
  }
  public clickedSegment() {
    this.segmSwitch = false;
    if (this._shopReportDataService._segmentChosen['segmentName']) {
      this.segmentButColor = 'primary';
      this.load = true;
      this.shopsData = [];
      this.getSegmentShopData()
    } else {
      this.segmentButColor = 'secondary';
      this.prepareParams()
    }
  }
  public switchMenu() {
    this.segmSwitch = true;
  }

  public showDinamics(mode, val) {
    this._shopReportDataService._shopChosen = {
      'olapCode': mode['olapCode'],
      'name': mode['name'],
      'shopNumb': mode['shopNumb'],
    };
    this.chartVal = val;
    this.dinamicChart = true
  }
  public hideDinamics(val) {
    this.dinamicChart = false
  }

  public clickShopSegm (item) {
    if (!item['segmentName']) {
      this._shopReportDataService._shopChosen = {
        'olapCode': (item.shop_code || item.olapCode).toString(),
        'shopNumb': item.shopNumb,
        'name': item.name
      };
      this._shopReportDataService._optSelect = 'shop';
    } else {
      this._shopReportDataService._segmentChosen = {
        'wareCode': item.wareCode.toString(),
        'segmentName': item.segmentName
      };
      this._shopReportDataService._optSelect = 'segment'
    }
    this.prepareParams();
  }

  private sortTableValues(a: Object, b: Object): number {
    if (this.shopsData) {
      let self = this;
      let tblVls = [];
      self.tableVals.forEach(function (value) {
        if (value['mode'] === self.shopMode) {
          tblVls = value['values']
        }
      });
      let resp: number = 0;
      let lastParam : number = tblVls.length - 1;
      if (a[tblVls[lastParam]] > b[tblVls[lastParam]]) {
        resp = -1
      } else if (a[tblVls[lastParam]] < b[tblVls[lastParam]]) {
        resp = 1
      } else {
        resp = 0
      }
      return resp
    }
  }

   private getSegmentsSqlite() {
    this._getSegmSqlite.get('sqlite')
      .then((res: any) => {
      res = JSON.parse(res);
      console.log(res);
        if (res['errMsg'] === 'Старые данные') {
          this.getSegments()
        } else {
          this._shopReportDataService._segmentsList = res.data['data'];
          this.getShopChosenData()
        }
      }).catch((e) => {
      this.getSegments()
    })
  }

  private getSegments() {
    this._getSegm.get('default')
      .then((res: any) => {
        res = JSON.parse(res);
        console.log(res);
        this._shopReportDataService._segmentsList = res.data;
        this.setSegmentsSqlite();
        this.getShopChosenData();
        // this.sortMethod();
      }).catch((e)=> {
      console.log('Не получилось забрать сегменты из базы')
    })
  }

   private setSegmentsSqlite() {
    this._setSegm.set(this._shopReportDataService._segmentsList)
      .then((res: any) => {
    }
      ).catch((e) => {
      console.log(e);
    })
  }

}
