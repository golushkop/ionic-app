import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DayPage } from './day';
import { ComponentsModule } from '../../components/components.module';
// import { SegmentsListComponent } from '../../components/segments-list/segments-list';

@NgModule({
  declarations: [
    DayPage,
  ],
  imports: [
    IonicPageModule.forChild(DayPage),
    ComponentsModule,
    // SegmentsListComponent
  ],
  exports: [
    DayPage
  ],
  providers: [
  ]
})
export class DayPageModule {}
