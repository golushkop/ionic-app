import {Component, ViewChild} from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { RequestBlockScreenService } from '../../services/request-block-screen.service';
import { ShopReportDataService } from '../../services/shop-report-data.service';
import { GetShopListService } from '../../services/get-shop-list.service';
import { GetSummaryCashDataService } from '../../services/get-summary-cash-data.service';
import { GetSaleTodayByParamsService } from "../../services/get-sale-today-by-params.service";
import { PlanExecutionChartComponent } from "../../components/plan-execution-chart/plan-execution-chart";
import {RestService} from "../../services/rest.service"
import {UrlModels} from "../../app/models/urls.model"
import {Presets} from "../../common/presets"

/**
 * Generated class for the TodayPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
  selector: 'page-today',
  templateUrl: 'today.html',
})
export class TodayPage {
  @ViewChild(PlanExecutionChartComponent)
  planExecutionChart: PlanExecutionChartComponent;

 public tableVals: Object = {'values': ['shopNumb', 'Count', 'Sum'], 'headers': ['ГМ', 'Чеки', 'Сумма ТО']};
 public buttons: Array<Object> = [{'icon': 'checkmark', 'color': 'light', 'name': 'ТО'}];
 public load: boolean;
 // public shopsData: Array<any>;
 public summaryData: Array<any>;
 // public fSummaryData: Array<any>;
 public segmSwitch: boolean;
 public shopMode: string;
 private _date: any;
 private _dateFrom: string;
 private _dateTo: string;
 public segmentButColor: string;
 // public summaryCashData: Array<any>;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public _requestBlockScreenService: RequestBlockScreenService,
              private platform: Platform,
              public _shopReportDataService: ShopReportDataService,
              private _getShopsListService: GetShopListService,
              private _getSummaryCash: GetSummaryCashDataService,
              private _getSaleToday: GetSaleTodayByParamsService,
              private _restServie: RestService
  ) {
    platform.ready().then(() => {
      // this.prepareParams();
      this.checkIfShopsList();
    })
  }
  ionViewCanEnter() {
    this._shopReportDataService._segmentChosen = this._shopReportDataService._segmentChosen ? this._shopReportDataService._segmentChosen : {};
  }
  ionViewDidEnter () {
    this._shopReportDataService._shopMode = 'ТО';
  }

  private prepareParams() {
    this.load= true;
    this.defineMenuButtons();
    this.defineTableVal();
    this.summaryData = [];
    this.segmentButColor = 'secondary';
    this._shopReportDataService._planChartToTime = true;
    this._shopReportDataService._dateSelector = 'today';
    this._date = new Date();
    this._dateFrom = this._date.getFullYear() + '-' + ('0' + (this._date.getMonth() + 1)).slice(-2) + '-' + '01';
    this._dateTo = this._date.getFullYear() + '-' + ('0' + (this._date.getMonth() + 1)).slice(-2) + '-' +
      new Date(this._date.getFullYear(), this._date.getMonth() + 1, 0).getDate().toString();
     if (!this._shopReportDataService) {
      this._shopReportDataService._curTime = ('0' + this._date.getHours()).slice(-2) + ':' + ('0' + this._date.getMinutes()).slice(-2);
    }
    this._requestBlockScreenService.maxCounter = this._shopReportDataService._shopsList.length;
    this._requestBlockScreenService.counter = 0;
    this._shopReportDataService._optSelect = this._shopReportDataService._optSelect || 'all';

      this._shopReportDataService._shopMode = 'ТО';
      if (this._shopReportDataService._optSelect === 'all') {
        this._requestBlockScreenService.maxCounter = 1;
        this.getSummaryCashData();
      }
      else if (this._shopReportDataService._optSelect === 'segment') {
        this._requestBlockScreenService.maxCounter = 1;
        this.getSaleToday()
      }
      else {
        this.getChosenShopData()
      }
  }

   private checkIfShopsList() {
    this.load = true;
    if (this._shopReportDataService._shopsList) {
      this.prepareParams();
    }
    else {
      this.getShopsList()
    }
  }
   private getShopsList() {
    this._restServie.get(UrlModels.apiGetShopsList, {}, Presets.settingsWithLocalStorage).then(
        (result: any) => {
          console.log(result)  // FIXME
          result = JSON.parse(result);
          let shopAr: Array<Object> = [];
          result['shops'].forEach(function (item: any) {
            shopAr.push({
              'olapCode': item.olapCode,
              'name': item.name,
              'shopNumb': parseInt(item.name.slice(-2))
            })
          });
          console.log('Shop list is downloaded');
          this._shopReportDataService._shopsList = shopAr;
          this.prepareParams();
        }).catch( (e)=> {
          console.log('Не удалось загрузить список сагазинов')
        }
    );
  }
  private getSummaryCashData() {
      this._getSummaryCash.get()
      .then(
        (res: any) => {
          res= JSON.parse(res);
          let self = this;
          if (res.errMsg) {
            res.data = {
              'olapCode': self._shopReportDataService._shopsList[self._requestBlockScreenService.counter]['olapCode'],
              'name': self._shopReportDataService._shopsList[self._requestBlockScreenService.counter]['name'],
              'shopNumb': self._shopReportDataService._shopsList[self._requestBlockScreenService.counter]['name'].slice(-2)
            }
          }
          else {
            res.data.forEach( function (value) {
              self._shopReportDataService._shopsList.forEach(function (shop) {
                if (shop['shopNumb'] === value['Shop']) {
                  value['name'] = shop['name'];
                  value['shopNumb'] = shop['name'].slice(-2)
                }
              });
            value['errMsg'] = '';
            });
          }
          this.summaryData = res.data;
          this._requestBlockScreenService.counter++;
          if (this._shopReportDataService._shopMode !== 'План') {
            // this.sortByValues('Sum');
          }
          console.log(this.summaryData);
          this.calculateValues();
          // this.getSummaryCashData();
        }).catch((e)=> {
        console.log('Не удается загрузить getSummaryCashData')
      })
    }

    private getSaleToday() {
    this._getSaleToday.get({'wareCode': this._shopReportDataService._segmentChosen['wareCode']})
      .then(
        (res: any) => {
          res = JSON.parse(res);
          let self = this;
          res.data = res.data || {
              'errMsg': 'В магазине нет данных за сегодня',
              'olapCode': self._shopReportDataService._shopsList[self._requestBlockScreenService.counter]['olapCode'],
              'name': self._shopReportDataService._shopsList[self._requestBlockScreenService.counter]['name']
          };
          if (res.data.length === 0) {
            res.data['errMsg'] = 'В магазине нет данных за сегодня'
          }
          res.data.forEach( function (value) {
            value['errMsg'] = '';
            self._shopReportDataService._shopsList.forEach(function (val) {
              if (value['Shop'] === val['shopNumb']) {
                value['name'] = val['name'];
                value['shopNumb'] = val['name'].slice(-2)
              }
            });
          });
          this.summaryData = res.data;
          this._requestBlockScreenService.counter++;
          console.log(this.summaryData);
          if (this._shopReportDataService._optSelect === 'segment') {
            // this.fSummaryData = this.summaryData;
            // this.sortByValues('amountper');
            this.calculateValues()
          } else {
            return;
          }
        }).catch((e)=> {
      console.log('Не удалось загрузить getSaleToday')
    })
  }

  private getChosenShopData() {
    this._requestBlockScreenService.maxCounter = 1;
    this._requestBlockScreenService.counter = 0;
    this._getSaleToday.get({'olapCode': this._shopReportDataService._shopChosen['olapCode']})
      .then(
        (res: any) => {
          res = JSON.parse(res);
          let self = this;
          res.data = res.data || {
              'errMsg': 'В магазине нет данных за сегодня',
              'olapCode': self._shopReportDataService._shopsList[self._requestBlockScreenService.counter]['olapCode']
          };
          if (res.data.length === 0) {
            res.data['errMsg'] = 'В магазине нет данных за сегодня'
          }
          res.data.forEach( function (value) {
            value['olapCode'] = self._shopReportDataService._shopsList[self._requestBlockScreenService.counter]['olapCode'];
            value['errMsg'] = '';
          });
          this.summaryData = res.data;
          // this.sortByValues('amountper');
          console.log(this.summaryData);
          this.calculateValues();
        }).catch((e) => {
      console.log('Не удалось загрузить getChosenShopData')
    })
  }

  private calculateValues() {
    this.summaryData.forEach(function (value) {
      value['shopNumb'] = parseInt(value['shopNumb']);
      value['Sum'] = value['Sum'] / 100
    });
    let self = this;
    this.summaryData.sort(function (a: Object, b: Object): number {
      return self.sortTableValues(a, b)
    });
    if (this.summaryData[0]) {
      this._shopReportDataService._curTime = this.summaryData[0]['Date'].slice(-5);
      if (!this._shopReportDataService._dateChosen) {
        this._shopReportDataService._dateChosen = this.summaryData[0]['Date'].slice(0, 10)
      }
    }
    this.load = false;
  }

  clickedSegment() {
    this.segmSwitch = false;
    if (this._shopReportDataService._segmentChosen['segmentName']) {
      this.segmentButColor = 'primary';
      this.load = true;
      this.summaryData = [];
      this._shopReportDataService._optSelect = 'segment';
      this.defineTableVal();
      this.getSaleToday()
    } else {
      this._shopReportDataService._optSelect = 'all';
      this.shopMode = 'ТО';
      this.segmentButColor = 'secondary';
      this.prepareParams()
    }
  }
  private defineMenuButtons() {
     this.buttons = [
       {'icon': 'logo-usd', 'color': 'primary', 'name': 'ТО'},
       {'icon': 'stats', 'color': 'secondary', 'name': 'График'},
    ]
  }
   private defineTableVal() {
    if (this.shopMode === 'ТО') {
      this.tableVals = {'values': ['shopNumb', 'Count', 'Sum'], 'headers': ['ГМ', 'Чеки', 'Сумма ТО']}
    } if (this._shopReportDataService._optSelect === 'segment') {
      this.tableVals = {'values': ['shopNumb', 'amount', 'amountper', 'cntarticul', 'cntpurchase'], 'headers': ['ГМ', 'ТО/Доля рынка р', 'Арт', 'Клиенты']}
     }
  }
  public changeMode(value) {
    let self = this;
    this.buttons.forEach(function (but) {
      if (value === but['name']) {
        but['color'] = 'primary';
        self.shopMode = but['name'];
      } else {
        but['color'] = 'secondary'
      }
    });
    if (this.shopMode === 'График') {
      this._shopReportDataService._shopMode = 'План'
    }
  }
  public changeChartMode(val) {
    this._shopReportDataService._planChartToTime = !this._shopReportDataService._planChartToTime;
    this.planExecutionChart.prepareParams('TodayPeriod');
  }
  public switchMenu() {
    this.segmSwitch = true;
  }
   private sortTableValues(a: Object, b: Object): number {
    if (this.summaryData) {
      let self = this;
      let resp: number = 0;
      let lastParam : number = self.tableVals['values'].length - 1;
      if (a[self.tableVals['values'][lastParam]] > b[self.tableVals['values'][lastParam]]) {
        resp = -1
      } else if (a[self.tableVals['values'][lastParam]] < b[self.tableVals['values'][lastParam]]) {
        resp = 1
      } else {
        resp = 0
      }
      return resp
    }
  }
  public chartIsReady() {
    this.load = false;
  }

}
