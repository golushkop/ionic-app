import { Component } from '@angular/core';

import { DayPage } from '../day/day';
import { MonthPage } from "../month/month";
import {TodayPage} from "../today/today";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = TodayPage;
  tab2Root = DayPage;
  tab3Root = MonthPage;

  constructor() {

  }
}
