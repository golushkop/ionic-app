import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import { GetShopListService } from '../../services/get-shop-list.service';
import { ShopReportDataService } from '../../services/shop-report-data.service';
import { RequestBlockScreenService } from '../../services/request-block-screen.service';
import { GetTotalShopReport } from '../../services/get-total-shop-report.service';
import { GetShopInfoBySegments } from '../../services/get-shop-info-by-segments.service';

/**
 * Generated class for the MonthPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-month',
  templateUrl: 'month.html',
})
export class MonthPage {
 private shopsList: Array<Object>;
  private params: Object;
  private _dateTo: string;
  private _dateFrom: string;
  private yesterday: any;
  private _date: any;
  private _dateComp: string;
  public shopsData: Array<any>;
  public shopMode: string;
  public tableVals: Object = {'values': [], 'headers': []};
  public load: boolean;
  public colorTest: string = 'light';
  public buttons: Array<Object> = [{'icon': 'checkmark', 'color': 'light', 'name': 'ТО'}];
  public dpMonthNames: Array<string> = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ];
  public dpShortMonthNames: Array<string> = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек' ];
  public segmSwitch: boolean;
  public segmentButColor: string;
  private counter: number;


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private _getShopsListService: GetShopListService,
              public _shopReportDataService: ShopReportDataService,
              public _requestBlockScreenService: RequestBlockScreenService,
              // private platform: Platform,
              private _getTotalShopReport: GetTotalShopReport,
              private platform: Platform,
              private _getShopInfoBySegm: GetShopInfoBySegments
  ) {
    platform.ready().then(() => {
      this._shopReportDataService._segmentChosen = this._shopReportDataService._segmentChosen ? this._shopReportDataService._segmentChosen : {};

    })
  }

  ionViewDidLoad() {
    if (this._shopReportDataService._monthChosen) {
      this.pickDate()
    } else {
      this.prepareDate()
    }
  }

  private prepareDate() {
    this._date = new Date();
    this.yesterday = new Date();
    this._shopReportDataService._dateSelector = 'month';
    this.yesterday.setDate(this.yesterday.getDate() - 1);
    this._dateTo = this.yesterday.getFullYear() + '-' + ('0' + (this.yesterday.getMonth() + 1)).slice(-2) +
      '-' + ('0' + this.yesterday.getDate()).slice(-2);
    if (this._date.getDate() === 1) {
      this._dateFrom = this._date.getFullYear() + '-' + ('0' + this._date.getMonth()).slice(-2) +
        '-' + ('0' + this._date.getDate()).slice(-2);
    } else {
      this._date.setDate(1);
      this._dateFrom = this._date.getFullYear() + '-' + ('0' + (this._date.getMonth() + 1)).slice(-2) +
        '-' + ('0' + this._date.getDate()).slice(-2);
    }
    this._shopReportDataService._monthChosen = this._dateTo.slice(0, 4) + '-' + this._dateTo.slice(5, 7);
    this.prepareParams()
  }

  private pickDate() {
    let curDate: string = ('0' + (this.yesterday.getMonth() + 1).toString()).slice(-2) + '-' + this.yesterday.getFullYear().toString();
     if (this._shopReportDataService._monthChosen.slice(-2) + '-' + this._shopReportDataService._monthChosen.slice(0, 4)
      !== curDate) {
      let maxDays = new Date(parseInt(this._shopReportDataService._monthChosen.slice(0, 4)),
        parseInt(this._shopReportDataService._monthChosen.slice(-2)), 0).getDate();
      this._dateTo = this._shopReportDataService._monthChosen.slice(0, 4) + '-' + this._shopReportDataService._monthChosen.slice(-2)
        + '-' + maxDays.toString();
      this._dateFrom = this._shopReportDataService._monthChosen.slice(0, 4) + '-' + this._shopReportDataService._monthChosen.slice(-2)
        + '-01';
      this.prepareParams();
    } else {
       this.prepareDate()
     }
  }

  private prepareParams() {
    this.load = true;
    this.segmentButColor = 'secondary';
    this._requestBlockScreenService.counter = 0;
    this.counter = 0;
    this._shopReportDataService._segmentChosen = this._shopReportDataService._segmentChosen ? this._shopReportDataService._segmentChosen : {};
    this.shopMode = this.shopMode ? this.shopMode : 'ТО';
    this.defineTableVal();
    this.defineMenuButtons();
    // this.load = true;
    this._dateComp = 'DD';
    this.shopsList = [];
    this.shopsData = [];
    this.params = {};
    this.getShopsList();
  }

  private defineTableVal() {
    if (this.shopMode === 'ТО') {
      this.tableVals = {'values': ['name', 'summa', 'summa_plan', 'summa_prog'], 'headers': ['ГМ', 'ТО', 'Факт/Пл', 'DD']}
    } else if (this.shopMode === 'Проникновение') {
      this.tableVals = {'values': ['name', 'customer_part', 'customer_part_prog'], 'headers': ['ГМ', 'Проникновение', 'DD']}
    } else if (this.shopMode === 'Клиенты') {
      this.tableVals = {'values': ['name', 'customer', 'customer_plan', 'customer_prog'], 'headers': ['ГМ', 'Клиенты', 'Факт/Пл', 'DD']}
    } else if (this.shopMode === 'Артикулы') {
      this.tableVals = {'values': ['name', 'quantity', 'quantity_plan', 'quantity_prog'], 'headers': ['ГМ', 'Артикулы', 'Факт/Пл', 'DD']}
    } else if (this.shopMode === 'Маржа') {
      this.tableVals = {'values': ['name', 'margin', 'margin_prog'], 'headers': ['ГМ', 'Маржа', 'DD']}
    };
    let self = this;
    if (this.shopsData) {
      this.shopsData.sort(function (a, b): number {
        return self.sortTableValues(a, b)
      })
    }
  }
  private defineMenuButtons() {
    this.buttons = [
      {'icon': 'done-all', 'color': 'secondary', 'name': 'Проникновение'},
      {'icon': 'people', 'color': 'secondary', 'name': 'Клиенты'},
      {'icon': 'logo-usd', 'color': 'primary', 'name': 'ТО'},
      {'icon': 'basket', 'color': 'secondary', 'name': 'Артикулы'},
      {'icon': 'calculator', 'color': 'secondary', 'name': 'Маржа'},
      // {'icon': 'menu', 'color': 'secondary', 'name': 'Сегменты'},
    ]
  }
 private getShopsList() {
    this._getShopsListService.getShopsList()
      .then(
        (result: any) => {
          let self = this;
          result = JSON.parse(result);
          console.log(result);
          result['shops'].forEach(function (item: any) {
            self.shopsList.push({
              'olapCode': item.olapCode,
              'name': item.name,
              'shopNumb': parseInt(item.name.slice(-2))
            })
          });
          this._shopReportDataService._shopsList = this.shopsList;
          this._requestBlockScreenService.counter = 0;
          this._requestBlockScreenService.maxCounter = this._shopReportDataService._shopsList.length;
          this.getTotalShopData();
        },
      ).catch((e)=> {
      console.log('Ошибка загрузки списка магазинов');
      if (this._shopReportDataService._shopsList) {
        this.getTotalShopData()
      } else {
        console.log ('не получается загрузить магазины')
      }
    })
  }
  public renewPage() {
    alert('renew');
    this.prepareParams()
  }

   private getTotalShopData() {
    if (this.counter === this._shopReportDataService._shopsList.length) {
      console.log('===========');
      console.log(this.shopsData);
      this.counter = 0;
      // this._shopReportDataService._optSelect = 'all';
      this.calculateValues();
    } else {
      this.params = {
        'olapCode': this._shopReportDataService._shopsList[this.counter]['olapCode'],
        'dateFrom': this._dateFrom,
        'dateTo': this._dateTo,
        'dateComp': this._dateComp,
    };
      this._getTotalShopReport.get(this.params)
        .then(
          (res: any) => {
            res = JSON.parse(res);
            res.data[0]['name'] = this._shopReportDataService._shopsList[this.counter]['name'];
            res.data[0]['shopNumb'] = this._shopReportDataService._shopsList[this.counter]['name'].slice(-2);
            res.data[0]['olapCode'] = this._shopReportDataService._shopsList[this.counter]['olapCode'];
            this.shopsData.push(res.data[0]);
            this.counter++;
            this._requestBlockScreenService.counter ++;
            this.getTotalShopData();
          },
          (err: any) => {
            this.shopsData.push({
              'data': [{}],
              'errMsg': err
            });
            this.counter++;
            this._requestBlockScreenService.counter ++;
            this.getTotalShopData();
          }
        );
    }
  }
   private getSegmentShopData() {
    if (this.counter === this._shopReportDataService._shopsList.length) {
      this.counter = 0;
      // this._shopReportDataService._optSelect = 'segment';
      this.calculateValues();
    }
    else {
      this.params = {
        'olapCode': this._shopReportDataService._shopsList[this.counter]['olapCode'],
        'dateFrom': this._dateFrom,
        'dateTo': this._dateTo,
        'dateComp': this._dateComp,
        'segmentCode': this._shopReportDataService._segmentChosen['wareCode']
      };
      this._getShopInfoBySegm.get(this.params)
        .then(
          (res: any) => {
            res = JSON.parse(res);
            if (!res.errMsg) {
              res.data[0]['name'] = this._shopReportDataService._shopsList[this.counter]['name'];
              res.data[0]['shopNumb'] = this._shopReportDataService._shopsList[this.counter]['name'].slice(-2);
              res.data[0]['olapCode'] = this._shopReportDataService._shopsList[this.counter]['olapCode'];
              this.shopsData.push(res.data[0]);
            }
            else {
              this.shopsData.push({
                'name': this._shopReportDataService._shopsList[this.counter]['name'],
                'shopNumb': this._shopReportDataService._shopsList[this.counter]['name'].slice(-2),
                'olapCode': this._shopReportDataService._shopsList[this.counter]['olapCode'],
                'errMsg': res.errMsg
              });
            }
            this._requestBlockScreenService.counter++;
            this.counter++;
            this.getSegmentShopData();
          },
        ).catch((e) => {
        console.log(e)
      })
    }
  }

  private calculateValues() {
    // this.load = false;
    let self = this;
    this.shopsData.sort(function (a: Object, b: Object): number {
      return self.sortTableValues(a, b)
    });
    this._requestBlockScreenService.counter = 0;
    this.load = false;

  }

  public changeMode(value) {
    let self = this;
    this.buttons.forEach(function (but) {
      if (value === but['name']) {
        but['color'] = 'primary';
        self.shopMode = but['name'];
      } else {
        but['color'] = 'secondary'
      }
    });
    this.defineTableVal();
  }

  public dateIsChanged() {
    this.pickDate()
  }

  private sortTableValues(a: Object, b: Object): number {
    if (this.shopsData) {
      let self = this;
      let resp: number = 0;
      let lastParam : number = self.tableVals['values'].length - 1;
      if (a[self.tableVals['values'][lastParam]] > b[self.tableVals['values'][lastParam]]) {
        resp = -1
      } else if (a[self.tableVals['values'][lastParam]] < b[self.tableVals['values'][lastParam]]) {
        resp = 1
      } else {
        resp = 0
      }
      return resp
    }
  }
   public switchMenu() {
    this.segmSwitch = true;
  }
   public clickedSegment() {
    this.segmSwitch = false;
    if (this._shopReportDataService._segmentChosen['segmentName']) {
      this.segmentButColor = 'primary';
      this.load = true;
      this.shopsData = [];
      this.getSegmentShopData()
    } else {
      this.segmentButColor = 'secondary';
      this.prepareParams()
    }
  }


}
