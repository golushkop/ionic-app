import {Component, EventEmitter, Input, Output} from '@angular/core';
import {RequestBlockScreenService} from "../../services/request-block-screen.service";
import {ShopReportDataService} from "../../services/shop-report-data.service";
import {GetTotalShopReport} from "../../services/get-total-shop-report.service";

/**
 * Generated class for the DinamicsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'dinamics',
  templateUrl: 'dinamics.html'
})
export class DinamicsComponent {
  @Output() returnBack= new EventEmitter();
  @Input() chartVal;
  @Input() _mode;

  public load: boolean;
  private dateComp: string;
  private shopsData: Array<any>;
  private shopsDataRev: Array<any>;
  private daysNumb: number;
  private _dateTo: string;
  private _dateFrom: string;
  private periodEnd: string;
  private date: any;
  private params: Object;
  private periodSt: string;
  data: any;
  options: any;


  text: string;

  constructor(public _requestBlockScreenService: RequestBlockScreenService,
              public _shopReportDataService: ShopReportDataService,
              private _getTotalShopReportService: GetTotalShopReport
  ) {
    this.prepareParams()
  }

  private prepareParams() {
    this.load = true;
    this.daysNumb = 7;
    this._requestBlockScreenService.maxCounter = this.daysNumb;
    this._requestBlockScreenService.counter = 0;
    this.dateComp = 'LFL';
    this.shopsData = [];
    this.shopsDataRev = [];
    this._dateTo = this._shopReportDataService._dateChosen;
    this._dateFrom = this._shopReportDataService._dateChosen;
    this.periodEnd = this._dateTo.slice(-2) + '.' + this._dateTo.slice(-5, -3);
    this.date = new Date(this._dateTo);
    this.getData();
  }

  private getData() {
    if (this._requestBlockScreenService.counter === this._requestBlockScreenService.maxCounter) {
      // this._shopReportDataService._optSelect = 'all';
      // this.calculateValues();
      let self = this;
      let i = 0;
      this.shopsData.forEach(function (value) {
        self.shopsDataRev[self.daysNumb - 1 - i] = value;
        i++
      });
      this.periodSt = this._dateFrom.slice(-2) + '.' + this._dateFrom.slice(-5, -3);
      this.getChart();
    }
    else {
      this.params = {
        'olapCode': this._shopReportDataService._shopChosen['olapCode'],
        'dateFrom': this._dateFrom,
        'dateTo': this._dateTo,
        'dateComp': this.dateComp,
    };
      this._getTotalShopReportService.get(this.params)
        .then(
          (res: any) => {
            res = JSON.parse(res);
            // res.data[0]['name'] = this._shopReportDataService._shopsList[this._requestBlockScreenService.counter]['name'];
            // res.data[0]['shopNumb'] = this._shopReportDataService._shopsList[this._requestBlockScreenService.counter]['name'].slice(-2);
            // res.data[0]['y'] = res.data[0][this._shopReportDataService._lflChosen];
            res.data[0]['y'] = res.data[0][this.chartVal];
            res.data[0]['name'] = this._shopReportDataService._shopChosen['name'];
            this.shopsData.push(res.data[0]);
            this.shopsData[this._requestBlockScreenService.counter]['period'] = this._dateTo;
            this.date.setDate(this.date.getDate() - 1);
            this._dateTo = this.date.getFullYear() + '-' + ('0' + (this.date.getMonth() + 1)).slice(-2) +
              '-' + ('0' + this.date.getDate()).slice(-2);
            this._dateFrom = this._dateTo;
            this._requestBlockScreenService.counter ++;
            this.getData();
          })
        .catch((err: any) => {
        this._requestBlockScreenService.counter ++;
        this.shopsData.push({
              'data': [{}],
              'errMsg': err
        });
        this.getData();
      });
    }
  }

  private getChart() {
    this.load = false;
    this.data = [{
          'key': 'Динамика показателя',
          'color': 'grey',
          'values': this.shopsDataRev
        }];
     this.options = {
      chart: {
        type: 'multiBarChart',
        height: window.outerHeight * 0.6,
        margin: {
          left: 30,
          right: 10,
          bottom: 20,
          top: 20,
        },
        x: function(p){
              return (p['period'].slice(-2) + '.' + p['period'].slice(-5, -3));
            },
        y: function(p){
          return p['y'];
        },
         barColor: function (d: any) {
          let dateX = new Date(d['period']);
          let dayX = dateX.getDay();
          if (dayX === 0 || dayX === 6) {
            return '#00a379'
          }
          else {
            return 'grey'
          }
         },
        showValues: true,
        showControls: false,
        showLegend: false,
        duration: 500,
        xAxis: {
          tickFormat: function (d) {
                return d
              },
           showMaxMin: false
        },
        yAxis: {
          tickFormat: function (d) {
                return d;
                },
          showMaxMin: false,
        },
        interactive: true,
        tooltips: true,
        tooltip: { contentGenerator: function (val) {
          return '<table><tr style="text-align: center"><td colspan="2"><b><u>' + val['data']['name'] + '</u></b></td></tr>' +
              '<tr><td colspan="2"></td></td>' +
              '<tr><td>' + 'LFL: ' + '</td><td><b>' + val['data']['y'].toFixed(2) + '</b></td></tr>' +
              '<tr><td>' + 'Дата: ' + '</td><td><b>' + val['data']['period'].slice(-2) + '.' + val['data']['period'].slice(-5, -3) + '</b></td></tr>' +
            '</table>'
        }
        }
      }
    }
  }
  public returnButton() {
    this.returnBack.emit('Return from dinamics')
  }

}
