import {Component, EventEmitter, Input, Output} from '@angular/core';
import { GetSegmentsListService } from '../../services/get-segments-list.service';
import { ShopReportDataService } from '../../services/shop-report-data.service';
import { SetSegmentsListService } from '../../services/set-segments.service';
import {GetSqliteSegmentsListService} from "../../services/get-sqlite-segments.service";

/**
 * Generated class for the SegmentsListComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'segments-list',
  templateUrl: 'segments-list.html'
})
export class SegmentsListComponent {
  @Output() newSegment = new EventEmitter();

  public listValues: Array<Object>;
  public load: boolean;


  constructor(public _getSegmSqlite: GetSegmentsListService,
              public _shopReportDataService: ShopReportDataService,
              public _setSegm: SetSegmentsListService,
              private _getSegm: GetSqliteSegmentsListService

  ) {
    this._shopReportDataService._segmentsList = this._shopReportDataService._segmentsList ?
      this._shopReportDataService._segmentsList : [] ;
    this.load = true;
    if (this._shopReportDataService._segmentsList.length === 0) {
      this.getSegmentsSqlite()
    } else {
      this.prepareData()
    }
  }
  ngOnInit() {
  }

  private getSegmentsSqlite() {
    this._getSegmSqlite.get('sqlite')
      .then((res: any) => {
      res = JSON.parse(res);
      console.log(res);
        if (res['errMsg'] === 'Старые данные') {
          this.getSegments()
        } else {
          this._shopReportDataService._segmentsList = res.data['data'];
          this.prepareData()
        }
      }).catch((e) => {
      this.getSegments()
    })
  }

  private getSegments() {
    this._getSegm.get('default')
      .then((res: any) => {
        res = JSON.parse(res);
        console.log(res);
        this._shopReportDataService._segmentsList = res.data;
        this.setSegmentsSqlite();
        this.prepareData();
        // this.sortMethod();
      }).catch((e)=> {
      console.log('Не получилось забрать сегменты из базы')
    })
  }

  private setSegmentsSqlite() {
    this._setSegm.set(this._shopReportDataService._segmentsList)
      .then((res: any) => {
    }
      ).catch((e) => {
      console.log(e);
      this.prepareData()
    })
  }

  private prepareData() {

    // let self = this;
    console.log('==========');
    console.log(this._shopReportDataService._segmentsList);
    this.listValues = [];
    let self = this;
    this._shopReportDataService._segmentsList.forEach(function (value) {
      value['firstNumb'] = parseInt(value['segmentName'].slice(0,3));
      let counter = 0;
      let firsNumb = value['segmentName'].slice(0, 1);
      if (self.listValues) {
        self.listValues.forEach(function (list) {
          if (list['firstNumb'] === firsNumb) {
            counter++
          }
        });
        if (counter === 0) {
          self.listValues.push({
            'firstNumb': firsNumb,
            'header': parseInt(firsNumb.toString() + '00')
          })
        }
      } else {
        self.listValues.push({
          'firstNumb': firsNumb,
          'header': parseInt(firsNumb.toString() + '00')
        })
      }
    });
    this._shopReportDataService._segmentsList.sort(function (a, b) {
      return self.sortSegmentsFunction(a, b)
    })
    this.load = false;
  }

  public segmentIsClicked(segment) {
    this._shopReportDataService._segmentChosen = segment;
    this.newSegment.emit(this._shopReportDataService._segmentChosen)
  }

 private sortSegmentsFunction(a, b): number {
    let resp = 0;
    if (a['firstNumb'] > b['firstNumb']) {
      resp = 1
    } else if (a['firstNumb'] < b['firstNumb']) {
      resp = -1
    } else {
      resp = 0
    }
    return resp
 }
 public pickAllSegments() {
    this._shopReportDataService._segmentChosen = {};
   this.newSegment.emit(this._shopReportDataService._segmentChosen)
 }


}
