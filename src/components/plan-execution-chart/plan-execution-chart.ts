import {Component, EventEmitter, Output} from '@angular/core';
import { ShopReportDataService } from '../../services/shop-report-data.service';
import {GetPlanExecutionService} from "../../services/get-plan-execution.service";
import {GetTotalShopReport} from "../../services/get-total-shop-report.service";
import {RequestBlockScreenService} from "../../services/request-block-screen.service";
import {GetDayService} from "../../services/get-day.service";
import {GetSummaryCashDataService} from "../../services/get-summary-cash-data.service";
declare let d3: any;

/**
 * Generated class for the PlanExecutionChartComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'plan-execution-chart',
  templateUrl: 'plan-execution-chart.html'
})
export class PlanExecutionChartComponent {
  @Output() isDownloaded = new EventEmitter();

  private _params: Object;
  private params: Object;
  private _daysOfWeek: Array<string>;
  private _date: any;
  private date: any;
  private _planExecution: Array <Object>;
  data: any;
  options: any;

  private count: number;
  private planCounter: number;

  private dateTo: string;
  private dateFrom: string;
  private dateComp: string;
  private pastDate: any;
  public planData: Array<any>;
  private dayInPast: string;
  private summaryCashData: Array<any>;
  public planDataNoMaxMin: Array<any>;
  private charData: Array<Object>;

  private indShop: number;
  private indShopMin: number;
  private indDayMonthMin: number;
  private indDayMonthAverage: number;
  private indDayMonth: number;
  private planTodIndShop: number;
  private planMonthIndShop: number;
  private normDataCount: number;
  private colorsMismatch: Array<Object>;
  private colors: Array<string>;

  private requestCounter: number;

  public load: boolean;


  constructor(public _shopReportDataService: ShopReportDataService,
              public _getPlanExecution: GetPlanExecutionService,
              private _getTotalShopReport: GetTotalShopReport,
              public _requestBlockSrceenService: RequestBlockScreenService,
              private _getDay: GetDayService,
              private _getSummaryCash: GetSummaryCashDataService
  ) {
    this.prepareParams('day')
  }

  ngOnInit() {

  }

  public prepareParams(mode) {
    this.load = true;
    console.log('start plan chart');
    this._daysOfWeek = ['Sunday', 'Weekdays', 'Weekdays', 'Weekdays', 'Weekdays', 'Weekdays', 'Saturday'];
    this._shopReportDataService._monthName = ['', 'Январь', 'Февраль', 'Март', 'Апрель', 'Май',
      'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
    this.colors = ['#B71C1C', '#EF5350', '#FF5722', '#E65100', '#FF9800', '#FFC107',
              '#FFEB3B', '#CDDC39', '#8BC34A', '#4CAF50', '#2E7D32'];
    this._date = new Date();
    this.count = 0;
    this.planCounter = 0;
    this.indDayMonth = 0;
    this.indDayMonthMin = 100;
    this.indDayMonthAverage = 0;
    this.normDataCount = 0;
    this.planData = [];
    this.summaryCashData = [];
    this._planExecution = [];
    this.colorsMismatch =[];
    this.dateComp = 'DD';
    this.requestCounter = 0;
    this._requestBlockSrceenService.counter = 0;
    if (this._shopReportDataService._shopMode === 'План') {
      this._requestBlockSrceenService.maxCounter = 4 * this._shopReportDataService._shopsList.length;
      this.date = new Date();
      if (this.date.getDate() !== 1) {
        this.dateFrom = this.date.getFullYear() + '-' + ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' + '01';
        this.dateTo = this.date.getFullYear() + '-' + ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' +
          new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate().toString();
      } else if (this.date.getDate() === 1) {
        this.dateTo = this.date.getFullYear() + '-' + ('0' + this.date.getMonth()).slice(-2) + '-' +
          new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate().toString();
        this.dateFrom = this.date.getFullYear() + '-' + ('0' + this.date.getMonth()).slice(-2) + '-' + '01';
      }
    } else {
      this._requestBlockSrceenService.maxCounter = 3 * this._shopReportDataService._shopsList.length;
      this.dateTo = this._shopReportDataService._dateChosen.slice(0, 7) + '-' +
        new Date(parseInt(this._shopReportDataService._dateChosen.split('-')[0]), parseInt(this._shopReportDataService._dateChosen.split('-')[1]), 0).getDate().toString();
      this.dateFrom = this._shopReportDataService._dateChosen.slice(0, 7) + '-' + '01';
    };
    this.getPlanData();
  }

   private getPlanExec() {
    this._params = {
      'day_type': this._daysOfWeek[this._date.getDay()],
      'cur_hrs': parseInt(this._shopReportDataService._curTime.slice(0, 2)),
      'cur_min': parseInt(this._shopReportDataService._curTime.slice(-2))
    };
    this._getPlanExecution.get(this._params).then(
      (res: any) => {
        res = JSON.parse(res);
        this._planExecution = res['data'];
        console.log('getPlanExec is ready');
        this.getChart();
    }).catch((e) => {
      console.log('Не удалось получить данные о выполнении плана');
      this.getChart()
    });
  }
  private getPlanData() {
    // if (this._requestBlockSrceenService.counter === this._shopReportDataService._shopsList.length) {
    if (this.count === this._shopReportDataService._shopsList.length) {
      if (this.planCounter === 0) {
        if (this._shopReportDataService._shopMode === 'План') {
          this.date = new Date();
        } else if (this._shopReportDataService._shopMode === 'ПланПериод') {
          this.date = new Date(parseInt(this.dateTo.split('-')[0]), parseInt(this.dateTo.split('-')[1]) - 1, parseInt(this.dateTo.split('-')[2]));
        }
        this.count = 0;
        this.dateComp = 'DD';
        this.pastDate = new Date(this.date.setFullYear(this.date.getFullYear() - 1));
        this.dateFrom = this.pastDate.getFullYear() + '-' + ('0' + (this.pastDate.getMonth() + 1)).slice(-2) + '-' + '01';
        this.dateTo = this.pastDate.getFullYear() + '-' + ('0' + (this.pastDate.getMonth() + 1)).slice(-2) + '-' +
          new Date(this.pastDate.getFullYear(), this.pastDate.getMonth() + 1, 0).getDate().toString();
        this.planCounter ++;
        this.getPlanData();
      } else if (this.planCounter === 1) {
        this.count = 0;
        this.getDay()
      } else {
        console.log('third is ready');
        if (this._shopReportDataService._shopMode === 'План') {
          this.getSummaryCashData()
        } else {
          this.count = 0;
          this.getPeriodCashData()
          // this.getSummaryCashData()
        }
      }
    // this._shopReportDataService._optSelect = 'all';
    }
    else {
      this.params = {
        'olapCode': this._shopReportDataService._shopsList[this.count]['olapCode'],
        'dateFrom': this.dateFrom,
        'dateTo': this.dateTo,
        'dateComp': this.dateComp,
    };
      this._getTotalShopReport.get(this.params)
        .then(
          (res: any) => {
            res = JSON.parse(res);
            if (this.planCounter === 0) {
              res.data[0]['name'] = this._shopReportDataService._shopsList[this.count]['name'];
              res.data[0]['shopNumb'] = this._shopReportDataService._shopsList[this.count]['name'].slice(-2);
              res.data[0]['olapCode'] = this._shopReportDataService._shopsList[this.count]['olapCode'];
              res.data[0]['monthToday'] = this._shopReportDataService._monthName[parseInt(this.dateFrom.slice(5, 7))] + ' '
                + this.dateFrom.slice(0, 4);
              this.planData.push(res.data[0]);
            } else if(this.planCounter === 1) {
              this.planData[this.count]['past_month_sum'] = res.data[0]['summa'];
              this.planData[this.count]['past_month_sum_plan'] = res.data[0]['summa_plan'];
              this.planData[this.count]['pastMonth'] = this._shopReportDataService._monthName[parseInt(this.dateFrom.slice(5, 7))] + ' '
                + this.dateFrom.slice(0, 4);
            } else {
              if (res.data[0]['customer'] > 0 && res.data[0]['customer'] < 100) {
                this.planData[this.count]['sum_in_past'] = 0;
                this.planData[this.count]['sum_in_past_plan'] = 0;
              } else {
                this.planData[this.count]['sum_in_past'] = res.data[0]['summa'];
                this.planData[this.count]['sum_in_past_plan'] = res.data[0]['summa_plan'];
              }
              this.planData[this.count]['pastDay'] = this.dateFrom.slice(-2) + '.' +
                this.dateFrom.slice(5, 7) + '.' + this.dateFrom.slice(0, 4);
            }
            this._requestBlockSrceenService.counter ++;
            this.count ++;
            this.getPlanData();
          }).catch((e)=> {
        console.log('Ошибка получения данных')
      });
    }
  }

  private getDay() {
    let curDay = '';
    if (this._shopReportDataService._shopMode === 'План') {
      this.date = new Date();
      curDay = this.date.getFullYear() + '-' + ( '0' + (this.date.getMonth() + 1)).slice(-2) + '-' +
        ('0' + this.date.getDate()).slice(-2);
    }
    else {
      curDay = this._shopReportDataService._dateChosen
    }
    console.log(curDay);
    console.log(this._shopReportDataService._shopMode);
    this._getDay.get({'date': curDay})
      .then((res: any) => {
        res = JSON.parse(res);
        this.dayInPast = res.data[0]['day'];
        this.planCounter ++;
        // this._requestBlockSrceenService.counter = 0;
        this.count = 0;
        this.dateFrom = this.dayInPast;
        this.dateTo = this.dayInPast;
        this.dateComp = 'LFL';
        console.log('getDay is ready');
        this.getPlanData();
      }).catch((e)=> {
      console.log('Не получается получить день')
    })
  }

   private getSummaryCashData() {
    if (this.requestCounter < 20) {
      this._getSummaryCash.get()
        .then(
          (res: any) => {
            res = JSON.parse(res);
            let self = this;
            if (res.errMsg) {
              res.data = {
                'olapCode': self._shopReportDataService._shopsList[this.count]['olapCode'],
                'name': self._shopReportDataService._shopsList[this.count]['name'],
                'shopNumb': self._shopReportDataService._shopsList[this.count]['name'].slice(-2)
              }
            } else {
              res.data.forEach(function (value) {
                self._shopReportDataService._shopsList.forEach(function (shop) {
                  if (shop['shopNumb'] === value['Shop']) {
                    value['name'] = shop['name'];
                    value['shopNumb'] = shop['name'].slice(-2)
                  }
                });
                value['errMsg'] = '';
              });
            }
            this.summaryCashData = res.data;
            this._requestBlockSrceenService.counter++;
            console.log('summary is ready');
            this.requestCounter = 0;
            this.calculateValues();
            // this.getSummaryCashData();
          }).catch((e) => {
        console.log('Не получается забрать CashSummary');
        this.requestCounter++;
        this.getSummaryCashData();
      })
    } else {
      alert('превышено количество запросов')
    }
    // }
  }

  private getPeriodCashData() {
    if (this.count === this._shopReportDataService._shopsList.length) {
      // this._shopReportDataService._optSelect = 'all';
      this.calculateValues();
    } else {
      this.params = {
        'olapCode': this._shopReportDataService._shopsList[this.count]['olapCode'],
        'dateFrom': this._shopReportDataService._dateChosen,
        'dateTo': this._shopReportDataService._dateChosen,
        'dateComp': this.dateComp,
    };
      console.log(this.params);
      this._getTotalShopReport.get(this.params)
        .then(
          (res: any) => {
            res = JSON.parse(res);
            res.data[0]['name'] = this._shopReportDataService._shopsList[this.count]['name'];
            res.data[0]['shopNumb'] = this._shopReportDataService._shopsList[this.count]['name'].slice(-2);
            res.data[0]['olapCode'] = this._shopReportDataService._shopsList[this.count]['olapCode'];
            this.summaryCashData.push(res.data[0]);
            this._requestBlockSrceenService.counter ++;
            this.count ++;
            this.getPeriodCashData();
          }).catch((e)=> {
            console.log('Не получается получить getPeriodCashData');
            this.summaryCashData.push({
              'data': [{}],
              'errMsg': e
            });
            this.getPeriodCashData();
      });
    }
  }

  private calculateValues() {
    console.log(this.planData);
    console.log(this.summaryCashData);
    let self = this;
    this.planData.forEach(function (value) {
      value['plan_growth'] = ((value['summa'] / value['summa_plan']) * 100) / value['past_month_sum'] ?
        ((value['summa'] / value['summa_plan']) * 100) / value['past_month_sum'] : 0;
      self.summaryCashData.forEach(function (scd) {
        if (scd['shopNumb'] === value['shopNumb']) {
          if (self._shopReportDataService._shopMode === 'План') {
            value['current_day_sum'] = scd['Sum'] / 100;
          }
          else {
            value['current_day_sum'] = scd['summa']
          }
          if (self._shopReportDataService._shopMode === 'План') {
            value['dateTime'] = scd['Date'].slice(8, 10) + '.' + scd['Date'].slice(5, 7) + ' ' + scd['Date'].slice(-5);
          }
          else {
            value['dateTime'] = self._shopReportDataService._dateChosen.slice(-2) + '.' + self._shopReportDataService._dateChosen.slice(-5, -3)
              + ' ' + self._shopReportDataService._dateChosen.slice(-2) + '.' + self._shopReportDataService._dateChosen.slice(-5, -3)
          }
        }
      });
      value['plan_for_today'] = value['sum_in_past'] * value['plan_growth'];

      // Проверка, если слишком большое значение по сравнению с прошлым
      if (value['plan_for_today'] / value['sum_in_past'] > 3) {
        value['mismatch'] = 'План на сегодня превышает ТО за прошлый период более чем на 300%';
        value['past_month_sum'] = 0;
        value['sum_in_past'] = 0
      }
      value['plan_execution_for_today'] = (((value['current_day_sum']) / value['plan_for_today']) * 100) ?
        (((value['current_day_sum']) / value['plan_for_today']) * 100) : 0;
      if (value['past_month_sum_plan'] !== 0 && value['sum_in_past'] !== 0) {
        if (self.indDayMonth < (value['plan_for_today'] /  (value['summa'] * 100 / value['summa_plan']))) {
          self.indShop = value['shopNumb'];
          self.planTodIndShop = value['plan_for_today'];
          self.planMonthIndShop = value['summa'] * 100 / value['summa_plan'];
          self.indDayMonth = self.planTodIndShop / self.planMonthIndShop;
        }
        if (self.indDayMonthMin > (value['plan_for_today'] /  (value['summa'] * 100 / value['summa_plan']))) {
          self.indShopMin = value['shopNumb'];
          self.indDayMonthMin = value['plan_for_today'] / (value['summa'] * 100 / value['summa_plan'])
        }
      }
      // }
    });

    // Убирает максимальные и минимальные значения
    this.planDataNoMaxMin = this.planData.filter(function (value) {
      if (value['shopNumb'] === self.indShop || value['shopNumb'] === self.indShopMin) {
        return false
      } else {
        return true
      }
    });

    // Считает средний процент прироста по всем магазинам не считая макисмального и минимального значения
    this.planDataNoMaxMin.forEach(function (avr) {
      if (avr['past_month_sum_plan'] !== 0 && avr['sum_in_past'] !== 0) {
        self.normDataCount ++;
        self.indDayMonthAverage += avr['plan_for_today'] / (avr['summa'] * 100 / avr['summa_plan'])
      }
    });
    this.indDayMonthAverage = this.indDayMonthAverage / this.normDataCount;

    // Считает значения выполнения плана на основе средних данных по всем магазинам, если значения за прошедший период равны нолю
    this.planData.forEach(function (val) {
      if (val['past_month_sum_plan'] === 0 || val['sum_in_past'] === 0) {
        val['plan_for_today'] = (val['summa'] * 100 / val['summa_plan']) * self.indDayMonthAverage;
        val['index_day_month'] = self.indDayMonthAverage;
        val['plan_execution_for_today'] = (val['current_day_sum'] / val['plan_for_today']) * 100;
        val['indShop'] = self.indShop;
        val['indShopPlanTod'] = self.planTodIndShop;
        val['indShopPlanMonth'] = self.planMonthIndShop
      }
    });
    // if (this._shopReportDataService._shopMode === 'План') {
    //   this._shopReportDataService._curTime = this.summaryCashData[0]['Date'].slice(-5);
    // }
    // else {
    //   this._shopReportDataService._curTime = '23:00'
    // }
    if (this._shopReportDataService._shopMode === 'План' && this._shopReportDataService._planChartToTime) {
      this.getPlanExec();
    }
    else {
      this.getChart()
    }
  }

  private getChart() {
    this.charData = [];
    let self = this;
    this.planData.forEach(function (value) {
      if (isFinite(value['plan_execution_for_today'])) {
        if (self._planExecution) {
          self._planExecution.forEach(function (pln) {
            if (pln['shopNumb'] === parseInt(value['shopNumb'])) {
              value['planExecutionPerHrs'] = pln['plan_execution'];
              let min: number = parseFloat(pln['day_start']) - parseInt(pln['day_start']);
              if (min > 0) {
                min = min * 60;
                value['day_start'] = ('0' + parseInt(pln['day_start'])).slice(-2) + ':' + min.toString()
              }
              else {
                value['day_start'] = ('0' + pln['day_start']).slice(-2) + ':00'
              }
            }
          });
          if (!value['planExecutionPerHrs']) {
            value['planExecutionPerHrs'] = 0;
          }
        }
        self.charData.push(value)
      } else {
        value['plan_execution_for_today'] = 0;
        self.charData.push(value)
      }
    });
    console.log(this._shopReportDataService._planChartToTime);
    console.log(this._shopReportDataService._shopMode);
    if (!this._shopReportDataService._planChartToTime || this._shopReportDataService._shopMode !== 'План') {
      this.charData.sort(function (a, b) {
        if (a['plan_execution_for_today'] > b['plan_execution_for_today']) {
          return -1
        }
        else if (a['plan_execution_for_today'] === b['plan_execution_for_today']) {
          return 0
        }
        else {
          return 1
        }
      });
    }
    else {
      let dif1: number;
      let dif2: number;
      this.charData.sort( function (a, b) {
        dif1 = (a['plan_execution_for_today'] / a['planExecutionPerHrs'] * 100) ?
          (a['plan_execution_for_today'] / a['planExecutionPerHrs'] * 100) : 0;
        if (!isFinite(dif1)) {
          dif1 = 0
        }
        dif2 = (b['plan_execution_for_today'] / b['planExecutionPerHrs'] * 100) ?
          (b['plan_execution_for_today'] / b['planExecutionPerHrs'] * 100) : 0;
        if (!isFinite(dif2)) {
          dif2 = 0
        }
        // }
        if (dif1 > dif2) {
          return -1
        }
        else if (dif1 === dif2) {
          return 0
        }
        else {
          return 1
        }
      })
    }
    this.data = [{
          'key': 'Выполнение плана',
          'color': 'grey',
          'values': this.charData
        }];
   if (!this._shopReportDataService._planChartToTime || this._shopReportDataService._dateSelector === 'day') {
      this.getChartByDays();
    }
    else {
      this.chartPerHrs()
    }
  }

  private getChartByDays() {
   this.load = false;
   let self = this;
   this.options = {
      chart: {
        type: 'multiBarHorizontalChart',
        height: (window.outerHeight * 0.75) || (window.innerHeight * 0.8),
        margin:{
          left: 40,
          right: 10,
          bottom: 20,
          top: 5,
        },
        barColor: function (d: any) {
          if (d['mismatch']) {
            let amount = 0;
            self.colorsMismatch.forEach(function (col) {
              if (col['dif'] === Math.ceil(d['planExecutionPerHrs'] - d['plan_execution_for_today'])) {
                amount ++
              }
            });
            if (amount === 0) {
              self.colorsMismatch.push({
                'dif': Math.ceil(d['planExecutionPerHrs'] - d['plan_execution_for_today']),
                'plan_execution_for_today': d['plan_execution_for_today'],
                'planExecutionPerHrs': d['planExecutionPerHrs']
              })
            }
            return 'red'
          }
          if (d['planExecutionPerHrs']) {
            let difference: number = Math.ceil(d['planExecutionPerHrs'] - d['plan_execution_for_today']);
            if (difference < 0 || difference === -0) {
              return self.colors [10]
            }
            else if (difference > 10) {
              return self.colors [0]
            }
            else {
              return self.colors[10 - difference]
            }
          }
          else {
            if (d['plan_execution_for_today'] >= 100) {
              return self.colors[10]
            }
            else {
              let str = parseInt(('0' + d['plan_execution_for_today'].toString().split('.')[0]).slice(-2).slice(0, 1));
              return self.colors[str]
            }
          }
        },
        x: function (d) {
          return d['shopNumb'];
        },
        y: function (d) {
          return d['plan_execution_for_today'];
        },
        showValues: true,
        showControls: false,
        showLegend: false,
        duration: 500,
        xAxis: {
          showMaxMin: false
        },
        yAxis: {
          tickFormat: function (d) {
            return d3.format(',.2f')(d);
          },
          showMaxMin: false,
        },
        interactive: false,
        tooltips: true,
        showAxis: true,
        tooltip: {
          contentGenerator: function (key) {
            if (key['data']['past_month_sum'] && key['data']['sum_in_past']) {
              return '<font-size ="10px">' +
                '<table>' +
                '<tr><td colspan="2" style="text-align: center"><b>' + key['data']['name'] + '</b></td></td>' +
                // '<tr style="border-bottom: 1px solid black" "><td colspan="2"></td></tr>' +
                '<tr><td>' + 'ТО на ' + key['data']['dateTime'].split(' ')[1] + ': ' + '</td>' +
                  '<td><b>' + (key['data']['current_day_sum']).toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</b></td></tr>' +
                '<tr><td colspan="2"></td></td>' +
                '<tr><td>' + 'План ТО ' +  key['data']['dateTime'].split(' ')[0] + ': ' + '</td>' +
                  '<td><b>' + key['data']['plan_for_today'].toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</b></td></tr>' +
                '<tr style="border-bottom: 1px solid black" "><td colspan="2"></td></tr>' +
                '<tr><td>' + key['data']['monthToday'] + ': ' + '</td>' +
                  '<td>' + (key['data']['summa'] / key['data']['summa_plan'] * 100).toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</td></tr>' +
                '<tr><td>' +  key['data']['pastMonth'] + ': ' + '</td>' +
                  '<td>' + key['data']['past_month_sum'].toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</td></tr>' +
                '<tr><td>' +  key['data']['pastDay'] + ': ' + '</td>' +
                  '<td>' + key['data']['sum_in_past'].toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</td></tr>' +
                // '<tr style="border-bottom: 1px solid black" "><td colspan="2"></td></tr>' +
                '</table>'
            } else if (key['data']['mismatch']) {
               return '<table>' +
                '<tr><td colspan="2" style="text-align: center"><b>' + key['data']['name'] + '</b></td></td>' +
                // '<tr style="border-bottom: 1px solid black" "><td colspan="2"></td></tr>' +
                '<tr><td>' + 'ТО на ' + key['data']['dateTime'].split(' ')[1] + ': ' + '</td>' +
                  '<td><b>' + (key['data']['current_day_sum']).toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</b></td></tr>' +
                '<tr><td>' + 'План ТО ' + key['data']['dateTime'].split(' ')[0] + ': ' + '</td>' +
                  '<td><b>' + key['data']['plan_for_today'].toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</b></td></tr>' +
                '<tr><td>' + key['data']['monthToday'] + ': ' + '</td>' +
                  '<td>' + (key['data']['summa'] / key['data']['summa_plan'] * 100).toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</td></tr>' +
                '<tr style="border-bottom: 1px solid black" "><td colspan="2"></td></tr>' +
                '<tr><td colspan="2"></td></td>' +
                 '<tr><td>' + 'День/Мес: ' + '</td>' +
                  '<td>' + (key['data']['index_day_month'] * 100).toFixed(2) + '%' + '</td></tr>' +
                 '<tr><td colspan="2"></td></td>' +
                 '<tr><td colspan="2">' + '*Проверьте ТО за ' + key['data']['pastDay'] + '<br>'
                + ' ' + 'План на сегодня превышает' + '<br>' + 'прошлогодний на 300%' + '</td>' +
                '</table>'
            } else {
              return '<font-size ="10px">' +
                '<table>' +
                '<tr><td colspan="2" style="text-align: center"><b>' + key['data']['name'] + '</b></td></td>' +
                // '<tr style="border-bottom: 1px solid black" "><td colspan="2"></td></tr>' +
                '<tr><td>' + 'ТО на ' + key['data']['dateTime'].split(' ')[1] + ': ' + '</td>' +
                  '<td><b>' + (key['data']['current_day_sum']).toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</b></td></tr>' +
                '<tr><td>' + 'План ТО ' + key['data']['dateTime'].split(' ')[0] + ': ' + '</td>' +
                  '<td><b>' + key['data']['plan_for_today'].toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</b></td></tr>' +
                '<tr><td>' + key['data']['monthToday'] + ': ' + '</td>' +
                  '<td>' + (key['data']['summa'] / key['data']['summa_plan'] * 100).toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</td></tr>' +
                '<tr style="border-bottom: 1px solid black" "><td colspan="2"></td></tr>' +
                '<tr><td colspan="2"></td></td>' +
                  '<tr><td>' + 'День/Мес: ' + '</td>' +
                  '<td>' + (key['data']['index_day_month'] * 100).toFixed(2) + '%' + '</td></tr>' +
                '</table>'
            }
          }
        }
      }
    };
  }

   private chartPerHrs() {
    let self = this;
    this.load = false;
     this.options = {
        chart: {
          fill: 'yellow',
          type: 'multiBarHorizontalChart',
          height: (window.outerHeight * 0.75) || (window.innerHeight * 0.8),
          margin:{
            left: 30,
            right: 10,
            bottom: 20,
            top: 5,
          },
          bars: {
            forceY:[0, this.charData[this.charData.length - 1]['plan_execution_for_today'] /
            this.charData[this.charData.length - 1]['planExecutionPerHrs'] * 100  + 200]
          },
          barColor: function (d: any) {
            // let colors = ['#B71C1C', '#EF5350', '#FF5722', '#E65100', '#FF9800', '#FFC107',
            //   '#FFEB3B', '#CDDC39', '#8BC34A', '#4CAF50', '#2E7D32'];
            if (d['mismatch']) {
              let dif = d['plan_execution_for_today'] / d['planExecutionPerHrs'] * 100;
              let amount = 0;
              self.colorsMismatch.forEach(function (col) {
                if (col === dif) {
                  amount ++
                }
              });
              if (amount === 0) {
                self.colorsMismatch.push({
                  'dif': dif,
                  'planExecutionPerHrs': d['planExecutionPerHrs']
                });
              }
              return 'red'
            }
            if (d['planExecutionPerHrs'] !== 0) {
              let difference: number = d['plan_execution_for_today'] / d['planExecutionPerHrs'] * 100;
              if (difference > 100) {
                return self.colors [10]
              } else if (difference < 10) {
                return self.colors [0]
              } else {
                return self.colors[parseInt(difference.toString().slice(0, 1))]
              }
            }
            else {
              return self.colors[0]
            }
          },
          x: function (d) {
            return d['shopNumb'];
          },
          y: function (d) {
            if (d['planExecutionPerHrs'] !== 0) {
              return (d['plan_execution_for_today'] / d['planExecutionPerHrs'] * 100) ?
                (d['plan_execution_for_today'] / d['planExecutionPerHrs'] * 100) : 0;
            // } else if(d['mismatch']) {
            //   return 100
            } else {
              return 0
            }
          },
          showValues: true,
          showControls: false,
          showLegend: false,
          duration: 500,
          xAxis: {
            showMaxMin: false,
          },
          yAxis: {
            tickFormat: function (d) {
              return d3.format(',.0f')(d);
            },
            showMaxMin: false,
            // tickValues: function (d, e) {
            //   let arr = [0];
            //   let tick: number = 0;
            //   let val = self.charData[0]['plan_execution_for_today'] /
            //     self.charData[0]['planExecutionPerHrs'] * 100;
            //   while (val > 0) {
            //     val = val - 25;
            //     if (val > 0) {
            //       tick = tick + 25;
            //       arr.push(tick)
            //     }
            //   }
            //   return arr
            // }
          },
          interactive: false,
          tooltips: true,
          showAxis: true,
          tooltip: {
            contentGenerator: function (key) {
              if (key['data']['past_month_sum']) {
                return '<font-size ="10px">' +
                  '<table>' +
                  '<tr><td colspan="2" style="text-align: center"><b>' + key['data']['name'] + '</b></td></td>' +
                  // '<tr style="border-bottom: 1px solid black" "><td colspan="2"></td></tr>' +
                  '<tr><td>' + 'ТО на ' + key['data']['dateTime'].split(' ')[1] + ': ' + '</td>' +
                    '<td><b>' + (key['data']['current_day_sum']).toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</b></td></tr>' +
                  '<tr><td colspan="2"></td></td>' +
                  '<tr><td>' + 'Начало рабочего дня: ' + '</td>' +
                    '<td><b>' + key['data']['day_start'] + '</b></td></tr>' +
                  '<tr><td>' + 'План ТО к ' +  key['data']['dateTime'].split(' ')[1] + ': ' + '</td>' +
                  '<td><b>' + ((key['data']['planExecutionPerHrs'] * key['data']['plan_for_today']) / 100).toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') +
                    '</b></td></tr>' +
                  '<tr><td>' + 'План ТО ' +  key['data']['dateTime'].split(' ')[0] + ': ' + '</td>' +
                    '<td><b>' + key['data']['plan_for_today'].toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</b></td></tr>' +
                  '<tr style="border-bottom: 1px solid black" "><td colspan="2"></td></tr>' +
                  '<tr><td>' + key['data']['monthToday'] + ': ' + '</td>' +
                    '<td>' + (key['data']['summa'] / key['data']['summa_plan'] * 100).toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</td></tr>' +
                  '<tr><td>' +  key['data']['pastMonth'] + ': ' + '</td>' +
                    '<td>' + key['data']['past_month_sum'].toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</td></tr>' +
                  '<tr><td>' +  key['data']['pastDay'] + ': ' + '</td>' +
                    '<td>' + key['data']['sum_in_past'].toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</td></tr>' +
                  // '<tr style="border-bottom: 1px solid black" "><td colspan="2"></td></tr>' +
                  '</table>'
              } else if (key['data']['mismatch']) {
                return '<table>' +
                  '<tr><td colspan="2" style="text-align: center"><b>' + key['data']['name'] + '</b></td></td>' +
                  // '<tr style="border-bottom: 1px solid black" "><td colspan="2"></td></tr>' +
                  '<tr><td>' + 'ТО на ' + key['data']['dateTime'].split(' ')[1] + ': ' + '</td>' +
                  '<td><b>' + (key['data']['current_day_sum']).toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</b></td></tr>' +
                  '<tr><td>' + 'План ТО ' + key['data']['dateTime'].split(' ')[0] + ': ' + '</td>' +
                  '<td><b>' + key['data']['plan_for_today'].toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</b></td></tr>' +
                  '<tr><td>' + key['data']['monthToday'] + ': ' + '</td>' +
                  '<td>' + (key['data']['summa'] / key['data']['summa_plan'] * 100).toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</td></tr>' +
                  '<tr style="border-bottom: 1px solid black" "><td colspan="2"></td></tr>' +
                  '<tr><td colspan="2"></td></td>' +
                  '<tr><td>' + 'День/Мес: ' + '</td>' +
                  '<td>' + (key['data']['index_day_month'] * 100).toFixed(2) + '%' + '</td></tr>' +
                  '<tr><td colspan="2"></td></td>' +
                  '<tr><td colspan="2">' + '*Проверьте ТО за ' + key['data']['pastDay'] + '<br>'
                  + ' ' + 'План на сегодня превышает' + '<br>' + 'прошлогодний на 300%' + '</td>' +
                  '</table>'
              } else {
                return '<font-size ="10px">' +
                  '<table>' +
                  '<tr><td colspan="2" style="text-align: center"><b>' + key['data']['name'] + '</b></td></td>' +
                  // '<tr style="border-bottom: 1px solid black" "><td colspan="2"></td></tr>' +
                  '<tr><td>' + 'ТО на ' + key['data']['dateTime'].split(' ')[1] + ': ' + '</td>' +
                    '<td><b>' + (key['data']['current_day_sum']).toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</b></td></tr>' +
                  '<tr><td>' + 'Начало рабочего дня: ' + '</td>' +
                    '<td><b>' + key['data']['day_start'] + '</b></td></tr>' +
                  '<tr><td>' + 'План ТО к ' +  key['data']['dateTime'].split(' ')[1] + ': ' + '</td>' +
                  '<td><b>' + ((key['data']['planExecutionPerHrs'] * key['data']['plan_for_today']) / 100).toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') +
                    '</b></td></tr>' +
                  '<tr><td>' + 'План ТО ' + key['data']['dateTime'].split(' ')[0] + ': ' + '</td>' +
                    '<td><b>' + key['data']['plan_for_today'].toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</b></td></tr>' +
                  '<tr><td>' + key['data']['monthToday'] + ': ' + '</td>' +
                    '<td>' + (key['data']['summa'] / key['data']['summa_plan'] * 100).toFixed(0).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ') + '</td></tr>' +
                  '<tr style="border-bottom: 1px solid black" "><td colspan="2"></td></tr>' +
                  '<tr><td colspan="2"></td></td>' +
                    '<tr><td>' + 'День/Мес: ' + '</td>' +
                    '<td>' + (key['data']['index_day_month'] * 100).toFixed(2) + '%' + '</td></tr>' +
                  '</table>'
              }
            }
          }
        }
      };
  }
  private isDownload() {
    this.isDownloaded.emit(true)
  }

  public ngOnDestroy(): void {
    try {
        document.getElementsByClassName('nvtooltip xy-tooltip')[0].remove();
    } catch (e) {}
  }

}
