import { Component } from '@angular/core';

/**
 * Generated class for the MenuComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'menu',
  templateUrl: 'menu.html'
})
export class MenuComponent {

  public dayMenuList: Array<Object>;

  constructor() {
  }
  ionViewDidLoad() {
    this.prepareParams()
  }

  private prepareParams() {
    this.dayMenuList = [
      {'menuName': 'Проникновение'},
      {'menuName': 'Клиенты'},
      {'menuName': 'Товарооборот'},
      {'menuName': 'Артикулы'},
      {'menuName': 'Маржа'},
      {'menuName': 'График'},
    ]
  }

}
