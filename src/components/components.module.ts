import { ModuleWithProviders, NgModule} from '@angular/core';
import { HashStorageService } from '../services/hash-storage.sevice';
// import { RestService } from '../services/rest.service';
import { GetShopListService } from '../services/get-shop-list.service';
import { RequestBlockScreenService } from '../services/request-block-screen.service';
import { ShopReportDataService } from '../services/shop-report-data.service';
import { GetTotalShopReport } from '../services/get-total-shop-report.service';
import { NumFormatPipe } from '../pipes/num-format/num-format';
import { SegmentsListComponent } from './segments-list/segments-list';
import { GetSegmentsListService } from '../services/get-segments-list.service';
import { SetSegmentsListService } from '../services/set-segments.service';
import { IonicPageModule } from 'ionic-angular';
import { GetShopInfoBySegments } from '../services/get-shop-info-by-segments.service';
import { PlanExecutionChartComponent } from './plan-execution-chart/plan-execution-chart';
import { GetPlanExecutionService } from '../services/get-plan-execution.service';
import { GetSummaryCashDataService } from '../services/get-summary-cash-data.service';
import { GetDayService } from '../services/get-day.service';
import { NvD3Module } from 'angular2-nvd3'
import { GetSaleTodayByParamsService } from '../services/get-sale-today-by-params.service';
import { RestService } from '../services/rest.service';
import { GetSqliteSegmentsListService } from '../services/get-sqlite-segments.service';
import { DinamicsComponent } from './dinamics/dinamics';
import { GetShopRepBySegmService } from '../services/get-shop-rep-by-segm.service';
import { MenuComponent } from "./menu/menu"


@NgModule({
	declarations: [NumFormatPipe,
    SegmentsListComponent,
    PlanExecutionChartComponent,
    DinamicsComponent,
    MenuComponent
  ],
	imports: [
	  IonicPageModule.forChild(ComponentsModule),
    NvD3Module
  ],
	exports: [NumFormatPipe,
    SegmentsListComponent,
    PlanExecutionChartComponent,
    DinamicsComponent,
    MenuComponent
  ],
  providers: [
    HashStorageService,
    // RestService,
    GetShopListService,
    RequestBlockScreenService,
    ShopReportDataService,
    GetTotalShopReport,
    GetSegmentsListService,
    SetSegmentsListService,
    GetShopInfoBySegments,
    GetPlanExecutionService,
    GetDayService,
    GetSummaryCashDataService,
    GetSaleTodayByParamsService,
    RestService,
    GetSqliteSegmentsListService,
    GetShopRepBySegmService
  ]
})
export class ComponentsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ComponentsModule,
      providers: [
        HashStorageService,
        // RestService,
        GetShopListService,
        RequestBlockScreenService,
        ShopReportDataService,
        GetTotalShopReport,
        GetSegmentsListService,
        SetSegmentsListService,
        GetShopInfoBySegments,
        GetPlanExecutionService,
        GetDayService,
        GetSummaryCashDataService,
        GetSaleTodayByParamsService,
        RestService,
        GetSqliteSegmentsListService,
        GetShopRepBySegmService
      ]
    };
  }
}
