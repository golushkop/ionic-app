import { Injectable } from '@angular/core';
import { UrlModels } from '../app/models/urls.model';
import { Presets } from "../common/presets";
import {RestService} from "./rest.service";


@Injectable()
export class GetDayService {

  constructor(private _restService: RestService) {
  }

  get(params) {
    return this._restService.get(UrlModels.apiGetDay, params, Presets.settingsWithLocalStorage)
  }
}
