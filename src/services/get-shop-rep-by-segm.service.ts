import { Injectable } from '@angular/core';

import { UrlModels } from '../app/models/urls.model';

import {RestService} from "./rest.service";
import {Presets} from "../common/presets";



@Injectable()
export class GetShopRepBySegmService {

  constructor(private _restServie: RestService
  ) { }

  get(params) {
    return this._restServie.get(UrlModels.apiSaleReport, params, Presets.settingsWithLocalStorage)
  }
}
