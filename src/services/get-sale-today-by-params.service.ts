import { Injectable } from '@angular/core';

import { Presets } from "../common/presets";
import {RestService} from "./rest.service";
import {UrlModels} from "../app/models/urls.model";


@Injectable()
export class GetSaleTodayByParamsService {

  constructor(private _restService: RestService) {
  }

  get(params) {
    return this._restService.get(UrlModels.apiShopReportToday, params, {})

    // return new Promise((resolve, reject) => {
    //   this.http.get(UrlModels.apiShopReportToday, params, {}).then((response: HTTPResponse) => {
    //     resolve(response.data)
    //   }).catch((e) => {
    //     console.log('Ошибка ' + e.error);
    //     console.log('Статус ' + e.status);
    //     reject(e)
    //   })
    //
    // })
  }
}
