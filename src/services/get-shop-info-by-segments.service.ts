import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs/Observable';
import { UrlModels } from '../app/models/urls.model';
// import { Presets } from '../common/presets';
import { HTTP, HTTPResponse } from '@ionic-native/http'
import { AppConfig } from '../app/config';
import { Presets } from "../common/presets";


@Injectable()
export class GetShopInfoBySegments {

  constructor(private http: HTTP,

  ) { }

  get(params) {
    return new Promise ((resolve, reject)=> {
      this.http.get(UrlModels.apiShopReport, params, Presets.settingsWithLocalStorage).then((response: HTTPResponse)=> {
        resolve(response.data)
        }).catch((e)=> {
        console.log('Ошибка ' + e.error);
        console.log('Статус ' + e.status);
        reject(e)
        })
    })
  }
}
