import { Injectable } from '@angular/core';
import { UrlModels } from '../app/models/urls.model';
import { Presets } from "../common/presets";
import {RestService} from "./rest.service";


@Injectable()
export class GetPlanExecutionService {

  constructor(private _restService: RestService,) {
  }

  get(params) {
    return this._restService.get(UrlModels.apiGetPlanExecution, params, {});
  //   return new Promise((resolve, reject) => {
  //     this.http.get(UrlModels.apiGetPlanExecution, params, {}).then((response: HTTPResponse) => {
  //       resolve(response.data)
  //     }).catch((e) => {
  //       console.log('Ошибка ' + e.error);
  //       console.log('Статус ' + e.status);
  //       reject(e)
  //     })
  //
  //   })
  }
}
