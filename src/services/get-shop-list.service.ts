import { Injectable } from '@angular/core';

import { UrlModels } from '../app/models/urls.model';

import {RestService} from "./rest.service";
import {Presets} from "../common/presets";



@Injectable()
export class GetShopListService {

  constructor(private _restServie: RestService
  ) { }

  getShopsList(){
    return this._restServie.get(UrlModels.apiGetShopsList, {}, Presets.settingsWithLocalStorage)
  }
}
