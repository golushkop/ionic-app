import { Injectable } from '@angular/core';
import { UrlModels } from '../app/models/urls.model';

import { Presets } from "../common/presets";
import {RestService} from "./rest.service";


@Injectable()
export class GetSqliteSegmentsListService {

  constructor(private _restService: RestService

  ) { }

  get(val) {
      console.log('segments from base');
      return this._restService.get(UrlModels.apiGetSegmentsList, {}, Presets.settingsWithLocalStorage)
  }
}
