import { Injectable } from '@angular/core';

@Injectable()
export class HashStorageService {

  private _storage: Array<Object>;

  constructor() {
    this._storage = [];
  }

  /**
   * Метод добавляет или обновляет данные в хранилище
   * @param hash хэш-код под которым хранится объект
   * @param data данные для помещения в хранилище
   */
  pushDataInStorage(hash: string, data: Object): void {
    const hashData: Object = {
      hash: hash,
      data: data,
      date: new Date()
    };

    this._storage.push(hashData);
  }

  /**
   * Метод возвращает объект хранения по хешу
   * @param hash хеш для определения ячейки хранилища
   */
  getDataFromStorage(hash: string): Object {
    for (const item of this._storage) {
      if (item['hash'] === hash) {
        if (((item['date'].getTime() - (new Date()).getTime()) / 1000 / 60 / 60) > 24) {
          // this.removeDataFromStorage(hash);
          this._storage.splice(this._storage.indexOf(item), 1);
          return null;
        }

        return item['data'];
      }
    }

    return null;
  }

  // private removeDataFromStorage(hash: string): void {
  //   for (const idx in this._storage) {
  //     if (this._storage[idx]['hash'] === hash) {
  //       this._storage.splice(parseInt(idx, 10), 1);
  //     }
  //   }
  // }

  /**
   * Метод очищает хранилище сервиса
   */
  clearService(): void {
    this._storage = [];
  }

}
