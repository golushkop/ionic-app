import { Injectable } from '@angular/core';

@Injectable()
export class RequestBlockScreenService {

  private _counter: number;
  get counter(): number { return this._counter; }
  set counter(value: number) { this._counter = value; }

  private _maxCounter: number;
  get maxCounter(): number { return this._maxCounter; }
  set maxCounter(value: number) { this._maxCounter = value; }

  constructor() {
    this._counter = 0;
    this._maxCounter = 0;
  }

  defaultSettings(): void {
    this._counter = 0;
    this._maxCounter = 0;
  }

}
