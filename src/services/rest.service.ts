import { Injectable } from '@angular/core';
import { RequestOptions, Response, URLSearchParams, Headers } from '@angular/http';

import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import {HashStorageService} from "./hash-storage.sevice";
import {Hash} from "../common/hash.enum";
import {Presets} from "../common/presets";
import { HTTP, HTTPResponse } from '@ionic-native/http'
import {AppConfig} from "../app/config";
import {resolveDep} from "@angular/core/src/view/provider"



@Injectable()
export class RestService {

  private _date: any;
  private _counter: number;
  private _reqAttempts: number;

  constructor(private _http: HTTP,
              private hashStrgService: HashStorageService) {
    this.prepareParams()
  }

  private prepareParams() {
    this._date = new Date();
    this._counter = 0;
    this._reqAttempts = 3;
  }

  public async get(url: string, params: Object = {}, settings: Object = Presets.defaultSettings): Promise<any> {

    if (!settings['lifetime']) {
      settings = {};
      this._http.useBasicAuth(AppConfig.username, AppConfig.password);
      try{
        return await this._http.get(url, params, settings)
      }
      catch (e) {
        this._counter++
        if (this._counter < this._reqAttempts) {
          this.get(url, params, settings)
        } else {
          console.error('Ошибка ' + e.error);
          console.log('Статус ' + e.status);
          return e
        }
      }
    } else {
      let dataFromStorage: any;
      dataFromStorage = await this.getFromLocalStorage(url, params, settings);
      console.log("dataFromStorage", dataFromStorage)  // FIXME
      if (dataFromStorage) {
        return dataFromStorage
      } else {
        console.log("getRequestAdd")  // FIXME
        let response = await this.getRequestAdd(url, params, settings)
        console.log(response)  // FIXME

      }
    }
  }

  private getRequestAdd(url: string, params: Object = {}, settings: Object = Presets.defaultSettings): any {
    this._http.useBasicAuth(AppConfig.username, AppConfig.password);
      return new Promise((resolve, reject) => {
        this._http.get(url, params, settings).then((response: HTTPResponse) =>{
          this.addToLocalStorage(response.data, url, params, settings);
          resolve(response.data)
        }).catch((e) => {
          if (this._counter < this._reqAttempts) {
            this.get(url, params, settings)
          } else {
            console.log('Ошибка ' + e.error);
            console.log('Статус ' + e.status);
            reject(e)
          }
        })
      })
  }

  private addToLocalStorage(resp, url, params, setting) {
    let key = url  + JSON.stringify(params);
    resp = JSON.parse(resp);
    resp['lifetime'] = setting['lifetime'];
    resp['timeAddedToStorage'] = this._date;
    let data = JSON.stringify(resp);
    localStorage.setItem(key, data)
  }

  private getFromLocalStorage(url, params, settings): any {
    return new Promise((resolve, reject) => {
      let hashObject: any = null;
      let key = url + JSON.stringify(params);
      hashObject = localStorage.getItem(key);
      if (hashObject)  {
        let jsonHashObj = JSON.parse(hashObject);
        let dateCreated: any = new Date(jsonHashObj['timeAddedToStorage']);
        let timeLeft: number = jsonHashObj['lifetime'];
        let timeDif: any = this._date - dateCreated;
        if (timeDif > timeLeft*60*60*100) {
          localStorage.removeItem(key);
          resolve (null)
        } else {
          resolve (hashObject)
        }
      } else {
        resolve (null)
      }
    }).catch((e: any) => {
      console.log(e)
    })
  }

}
