import { Injectable, Output, EventEmitter } from '@angular/core';
@Injectable()
export class ShopReportDataService {

  private _mode: string;
  get _shopMode(): string {return this._mode}
  set _shopMode (value: string) {this._mode = value}

  private _maxDayVal: string;
  get maxDay(): string {return this._maxDayVal}
  set maxDay (value: string) {this._maxDayVal = value}

  private _dateSelectorVal: string;
  get _dateSelector (): string { return this._dateSelectorVal}
  set _dateSelector (value: string) {this._dateSelectorVal = value}

  private _curTimeVal: string;
  get _curTime (): string { return this._curTimeVal}
  set _curTime (value: string) {this._curTimeVal = value}

  private _dateChosenVal: string;
  get _dateChosen (): string {return this._dateChosenVal}
  set _dateChosen (value: string) {this._dateChosenVal = value}

  private _prevDateChosenVal: string;
  get _prevDateChosen (): string {return this._prevDateChosenVal}
  set _prevDateChosen (value: string) {this._prevDateChosenVal = value}

  private _monthChosenVal: string;
  get _monthChosen (): string {return this._monthChosenVal}
  set _monthChosen (value: string) {this._monthChosenVal = value}

  private _segmentChosenVal: Object;
  get _segmentChosen (): Object {return this._segmentChosenVal}
  set _segmentChosen (value: Object) {this._segmentChosenVal = value}

  private _shopsListVal: Array<Object>;
  get _shopsList (): Array<Object> {return this._shopsListVal}
  set _shopsList (value: Array<Object>) {this._shopsListVal = value}

  private _segmentsListVal: Array<Object>;
  get _segmentsList (): Array<Object> {return this._segmentsListVal}
  set _segmentsList (value: Array<Object>) {this._segmentsListVal = value}

  private _optionsSelectorVal: string;
  get _optSelect (): string {return this._optionsSelectorVal}
  set _optSelect (value: string) {this._optionsSelectorVal = value}

  private _shopChosenVal: Object;
  get _shopChosen (): Object {return this._shopChosenVal}
  set _shopChosen (value: Object) {this._shopChosenVal = value}

  private _planDataVal: Array<any>;
  get _planData (): Array<Object> {return this._planDataVal}
  set _planData (value: Array<Object>) {this._planDataVal = value}

  private _yOffsetVal: number;
  get _yOffset (): number {return this._yOffsetVal}
  set _yOffset (value: number) {this._yOffsetVal = value}

  private _monthNameAr: Array<string>;
  get _monthName (): Array<string> {return this._monthNameAr}
  set _monthName (value: Array<string>) {this._monthNameAr = value}

  private _lflDinamicsVal: boolean;
  get _lflDinamics (): boolean {return this._lflDinamicsVal}
  set _lflDinamics (value: boolean) {this._lflDinamicsVal = value}

  private _sortCheckerVal: boolean;
  get _sortChecker (): boolean {return this._sortCheckerVal}
  set _sortChecker (value: boolean) {this._sortCheckerVal = value}

  private _shopLflDinVal: Object;
  get _shopLflDin (): Object {return this._shopLflDinVal}
  set _shopLflDin (value: Object) {this._shopLflDinVal = value}

  private _lflChosenVal: string;
  get _lflChosen (): string {return this._lflChosenVal}
  set _lflChosen (value: string) {this._lflChosenVal = value}

  private _planChartToTimeVal: boolean;
  get _planChartToTime (): boolean {return this._planChartToTimeVal}
  set _planChartToTime (value: boolean) {this._planChartToTimeVal = value}

  private _loaderVal: boolean;
  get loader (): boolean {return this._loaderVal}
  set loader (value: boolean) {this._loaderVal = value}

  // public getPlanChart (): Subject<any> {
  //   return this._planChartToTimeVal
  // }

  constructor() { }

}
