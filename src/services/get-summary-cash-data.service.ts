import { Injectable } from '@angular/core';
import { UrlModels } from '../app/models/urls.model';
import { Presets } from "../common/presets";
import {RestService} from "./rest.service";


@Injectable()
export class GetSummaryCashDataService {

  constructor(private _restService: RestService) {
  }

  get() {
    return this._restService.get(UrlModels.apiSummaryCashReport, {}, {});
    // return new Promise((resolve, reject) => {
    //   this.http.get(UrlModels.apiSummaryCashReport, {}, {}).then((response: HTTPResponse) => {
    //     resolve(response.data)
    //   }).catch((e) => {
    //     console.log('Ошибка ' + e.error);
    //     console.log('Статус ' + e.status);
    //     reject(e)
    //   })
    //
    // })
  }
}
