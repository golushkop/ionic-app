import { Injectable } from '@angular/core';
import { UrlModels } from '../app/models/urls.model';
import { Presets } from "../common/presets";
import {RestService} from "./rest.service";


@Injectable()
export class SetSegmentsListService {

  constructor(private _restService: RestService

  ) { }

  set(segments) {
    return this._restService.get(UrlModels.apiSetSegments, {data: segments}, Presets.settingsWithLocalStorage)
    // return new Promise ((resolve, reject)=> {
    //   this.http.get(UrlModels.apiSetSegments, {data: segments}, Presets.settingsWithLocalStorage).then((response: HTTPResponse)=> {
    //     resolve(response.data)
    //     }).catch((e)=> {
    //     console.log('Ошибка ' + e.error);
    //     console.log('Статус ' + e.status);
    //     reject(e)
    //     })
    //
    // })
  }
}
